package com.adon.tuk.db

import androidx.room.TypeConverter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import java.lang.Exception

class DBConverters {

    @TypeConverter
    fun toDate(value: Long?): LocalDateTime? {
        return if (value==null) null
        else LocalDateTime.ofEpochSecond(value, 0, ZoneOffset.UTC)
    }

    @TypeConverter
    fun fromDate(date: LocalDateTime?): Long? {
        return date?.toEpochSecond(ZoneOffset.UTC)
    }

    @TypeConverter
    fun to(v: String?): JSONObject? {
        return if (v==null) null
        else return try {
            JSONObject(v)
        } catch (e: Exception) {
            null
        }
    }

    @TypeConverter
    fun from(v: JSONObject?): String? {
        return v?.toString()
    }

}