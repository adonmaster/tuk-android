package com.adon.tuk

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.jmodels.JAd
import com.adon.tuk.rmodels.DriverDummyRModel
import com.adon.tuk.structs.AdRequestStruct

class AdVM: ViewModel() {

    val loading = MutableLiveData(false)

    var ad: JAd? = null

}