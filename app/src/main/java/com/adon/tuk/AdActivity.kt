package com.adon.tuk

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import com.adon.tuk.databinding.ActivityAdBinding
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.Intercept
import com.adon.tuk.helpers.InterceptListener
import com.adon.tuk.helpers.ter
import com.adon.tuk.rmodels.DriverDummyRModel
import com.adon.tuk.structs.AdRequestStruct
import com.adon.tuk.frags.ad.FragAdDriverConfirm
import com.adon.tuk.frags.ad.FragAdRequest
import com.adon.tuk.frags.ad.FragAdWait
import com.adon.tuk.jmodels.JAd
import com.adon.tuk.repo.Repo

class AdActivity : AppCompatActivity(), InterceptListener {

    companion object
    {
        var shared: AdActivity? = null

        fun startRequest(activity: Activity, request: AdRequestStruct)
        {
            val intent = Intent(activity, AdActivity::class.java)
            intent.putExtra("mode", "request")
            intent.putExtra("request", request)
            activity.startActivity(intent)
        }

        fun startAd(activity: Activity) {
            val intent = Intent(activity, AdActivity::class.java)
            intent.putExtra("mode", "ad")
            activity.startActivity(intent)
        }
    }


    //
    private lateinit var vm: AdVM
    private lateinit var ui: ActivityAdBinding

    //
    private lateinit var appBarConfig: AppBarConfiguration
    private lateinit var navController: NavController

    //
    private val cMode get() = intent.getStringExtra("mode")
    private val cRequest get() = intent.getParcelableExtra<AdRequestStruct>("request")

    //
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        vm = ViewModelProvider(this)[AdVM::class.java]
        ui = ActivityAdBinding.inflate(layoutInflater)
        setContentView(ui.root)

        if (cMode == "request") {
            supportFragmentManager.commit {
                add(R.id.frag_container, FragAdRequest::class.java, bundleOf("request" to cRequest))
            }
        } else if (cMode == "ad") {
            supportFragmentManager.commit {
                add(R.id.frag_container, FragAdWait::class.java, null)
            }
        }

    }

    fun outNavigateToAdWait(model: JAd)
    {
        Repo.userPref.putStatus("ad")
        vm.ad = model

        Act.fragReplace(supportFragmentManager, R.id.frag_container, FragAdWait::class.java)
    }

    fun outNavigateToDriverConfirm(model: DriverDummyRModel)
    {
        assert(false)

        Act.fragReplace(supportFragmentManager, R.id.frag_container, FragAdDriverConfirm::class.java)
    }

    fun outFcmNotify(data: Map<String, String>) {
        val payload = (data["payload"] ?: "").split(":")
        when(payload[0]) {
            "ad_remove" -> FragAdWait.shared?.outRequestAd()
            "ad_proposal" -> FragAdWait.shared?.outRequestProposal()
        }
    }

    override fun intercept(key: String): Any? {
        if (key=="AdActivity@isActive") return true
        return null
    }

    override fun onStart() {
        super.onStart()
        shared = this
    }

    override fun onStop() {
        super.onStop()
        shared = null
    }

    override fun onBackPressed() {
        finish()
    }

}