package com.adon.tuk.dialogs

import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.adon.tuk.R

abstract class DialogBase: DialogFragment() {

    override fun onResume()
    {
        val window = dialog!!.window!!

        val w = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.windowManager.currentWindowMetrics.bounds.width()
        } else {
            val size = Point()
            val display = window.windowManager.defaultDisplay
            display.getSize(size)
            size.x
        }

        window.setLayout((w * .95).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)

        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let { restoreUI(it) }
    }

    protected open fun restoreUI(savedInstanceState: Bundle) {}

    override fun onSaveInstanceState(outState: Bundle) {
        saveUI(outState)
        super.onSaveInstanceState(outState)
    }

    protected open fun saveUI(outState: Bundle) {}

    override fun getTheme() = R.style.RoundedCornersDialog

}