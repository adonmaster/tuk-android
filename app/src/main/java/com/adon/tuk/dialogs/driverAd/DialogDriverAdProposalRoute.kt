package com.adon.tuk.dialogs.driverAd

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.databinding.DialogDriverAdProposalRouteBinding
import com.adon.tuk.dialogs.DialogDriverAdVM

class DialogDriverAdProposalRoute: Fragment() {

    //
    private val vm by lazy { ViewModelProvider(requireActivity())[DialogDriverAdVM::class.java] }
    private var _ui: DialogDriverAdProposalRouteBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogDriverAdProposalRouteBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui.addressOrigin.text = vm.ad?.origin_s
        ui.addressDestination.text = vm.ad?.destination_s
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }
}