package com.adon.tuk.dialogs.driverAd

import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.Img
import com.adon.tuk.helpers.RvData
import com.adon.tuk.helpers.Str
import com.adon.tuk.jmodels.JProposal
import de.hdodenhof.circleimageview.CircleImageView

class DialogDriverAdProposalAdapter(private val listener: Listener): RecyclerView.Adapter<DialogDriverAdProposalAdapter.VH>() {

    //
    private val data = RvData<JProposal, VH>(this)
        .setCbIsSame { a, b -> a.price == b.price }


    //
    fun update(data: List<JProposal>) {
        this.data.update(data)
    }


    fun updateSingle(proposal: JProposal) {
        data.updateSingle(proposal)
    }

    override fun getItemCount(): Int {
        val n = data.items().size
        listener.onEmpty(n == 0)
        return n
    }

    //
    override fun getItemId(position: Int): Long {
        return data.items()[position].id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_driver_ad_proposal, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = data.items()[position]

        Img.loadAvatar(m.driver_avatar_thumb, holder.img_avatar)

        holder.lbl_title.text = "${m.driver_name_first} ${m.driver_name_last}"

        holder.lbl_price.text = "R$ " + Str.number(m.price)
    }

    //

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val img_avatar: CircleImageView by lazy { itemView.findViewById(R.id.img_avatar) }
        val lbl_title: TextView by lazy { itemView.findViewById(R.id.lbl_title) }
        val lbl_price: TextView by lazy { itemView.findViewById(R.id.lbl_price) }

        init {
            itemView.setOnClickListener {
                val p = absoluteAdapterPosition
                listener.onSelected(data.items()[p], p)
            }
        }

    }

    interface Listener {
        fun onEmpty(option: Boolean)
        fun onSelected(model: JProposal, position: Int)
    }
}