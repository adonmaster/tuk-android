package com.adon.tuk.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.adon.tuk.databinding.DialogAdDriverHireBinding
import com.adon.tuk.dialogs.adHire.DialogAdDriverHireCar
import com.adon.tuk.dialogs.adHire.DialogAdDriverHireComments
import com.adon.tuk.dialogs.adHire.DialogAdDriverHireDetail
import com.adon.tuk.helpers.*
import com.adon.tuk.rmodels.DriverDummyRModel
import com.google.android.material.tabs.TabLayoutMediator

class DialogAdDriverHire: DialogBase() {

    companion object {
        fun show(fm: FragmentManager?, driver: DriverDummyRModel) {
            if (fm == null) return
            val dialog = DialogAdDriverHire()
            dialog.arguments = bundleOf("model" to driver)
            dialog.isCancelable = false

            dialog.show(fm, "DialogAdDriverHire")
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(l: Listener) { listeners.add(l) }
        fun detach(l: Listener) { listeners.remove(l) }
    }


    //
    private lateinit var vm: DialogAdDriverHireVM
    private var _ui: DialogAdDriverHireBinding? = null
    private val ui get() = _ui!!

    //
    private lateinit var uiPageChanger: ViewPager2.OnPageChangeCallback

    //
    private val cModel get() = requireArguments().getParcelable<DriverDummyRModel>("model")!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vm = ViewModelProvider(this)[DialogAdDriverHireVM::class.java]
        _ui = DialogAdDriverHireBinding.inflate(inflater, container, false)
        dialog?.setTitle("Motorista")
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        Img.loadAvatar(cModel.avatar, ui.imgAvatar)

        ui.lblTitle.text = cModel.name
        ui.lblLike.text = Str.plural(cModel.like, "0 likes", "1 like", "$0 likes")
        ui.lblDislike.text = Str.plural(cModel.dislike, "0 likes", "1 like", "$0 likes")

        val progressLikeDislike = cModel.like.toFloat() / (cModel.like + cModel.dislike) * 100
        ui.pgLikeDislike.progress = progressLikeDislike.toInt()

        vm.loading.observe(this) {
            ui.progress.isVisible = it
            ui.btnSubmit.isEnabled = !it
            ui.btnCancel.isEnabled = !it
        }

        //
        setupPager()

        ui.btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        ui.btnSubmit.setOnClickListener {
            vm.loading.value = true
            Task.main(1500) {
                vm.loading.value = false

                Task.main { listeners.forEach { it.dialogAdDriverHireOnConfirm(cModel) } }
                dialog?.dismiss()
            }
        }
    }

    private fun setupPager()
    {
        val pages = arrayOf(
            "DETALHE" to { DialogAdDriverHireDetail() },
            "VEICULO" to { DialogAdDriverHireCar() },
            "POSITIVO (3)" to { DialogAdDriverHireComments.f(true) },
            "NEGATIVO (2)" to { DialogAdDriverHireComments.f(false) },
        )
        val adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount() = pages.size
            override fun createFragment(position: Int) = pages[position].second()
        }
        ui.pager.adapter = adapter
        uiPageChanger = object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                ui.pager.layoutParams.height = ter(
                    position > 0,
                    Act.dpToPixel(requireContext(), 200),
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                ui.pager.requestLayout()
            }
        }

        val tabLayoutMediator = TabLayoutMediator(ui.tabLayout, ui.pager, true) { tab, position ->
            tab.text = pages[position].first
        }
        tabLayoutMediator.attach()
    }

    override fun onStart() {
        super.onStart()
        ui.pager.registerOnPageChangeCallback(uiPageChanger)
    }

    override fun onStop() {
        super.onStop()
        ui.pager.unregisterOnPageChangeCallback(uiPageChanger)
    }

    //
    interface Listener {
        fun dialogAdDriverHireOnConfirm(model: DriverDummyRModel)
    }
}