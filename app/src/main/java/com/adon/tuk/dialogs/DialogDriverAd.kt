package com.adon.tuk.dialogs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.adon.tuk.R
import com.adon.tuk.databinding.DialogDriverAdBinding
import com.adon.tuk.dialogs.driverAd.DialogDriverAdProposal
import com.adon.tuk.dialogs.driverAd.DialogDriverAdProposalRoute
import com.adon.tuk.frags.FragNone
import com.adon.tuk.frags.driver_request.FragDriverRequestCar
import com.adon.tuk.frags.driver_request.FragDriverRequestDetail
import com.adon.tuk.frags.driver_request.FragDriverRequestLegal
import com.adon.tuk.frags.driver_request.FragDriverRequestLicense
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JAd
import com.adon.tuk.jmodels.JProposal
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.lang.RuntimeException

class DialogDriverAd: DialogBase() {

    //
    companion object {
        fun show(fm: FragmentManager?) {
            if (fm == null) return
            DialogDriverAd().show(fm, "DialogDriverAd")
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(l: Listener) { listeners.add(l) }
        fun detach(l: Listener) { listeners.remove(l) }

        private var shared: DialogDriverAd? = null

        fun validateInList(list: List<JAd>) {
            shared?.let { frag ->
                val index = list.indexOfFirst { it.id == frag.cModel.id }
                if (index == -1) frag.dismiss()
            }
        }
    }

    //
    private val vm by lazy { ViewModelProvider(requireActivity())[DialogDriverAdVM::class.java] }
    private var _ui: DialogDriverAdBinding? = null
    private val ui get() = _ui!!

    //
    private val cModel by lazy {
        if (listeners.isEmpty()) throw RuntimeException("faça o attach do fragment")
        listeners.last().dialogDriverAdRequest()
    }

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vm.ad = cModel
        _ui = DialogDriverAdBinding.inflate(inflater, container, false)
        dialog?.setTitle("Proposta")
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui.lblTitle.text = cModel.user_name
        Img.loadAvatar(cModel.user?.avatar_thumb, ui.imgAvatar)

        setupPager()

        val minsList = listOf("5 mins" to 5, "10 mins" to 10, "15 mins" to 15, "20 mins" to 20, "30 mins" to 30, "45 mins" to 45, "1 hora" to 60)
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, minsList.map { it.first })
        ui.edArrival.setAdapter(adapter)
        ui.edArrival.setText("5 mins", false)
        vm.arrival = 5
        ui.edArrival.setOnItemClickListener { _, _, i, _ ->
            vm.arrival = minsList.find { it.first == minsList[i].first }!!.second
        }

        vm.price.value = cModel.price
        Act.setupCalculator(vm.price, 10.0, ui.btnValueMinus5, ui.btnValueMinus, ui.btnValuePlus, ui.btnValuePlus5)
        vm.price.observe(this) {
            ui.lblPrice.text = "R$ " + Str.number(it)
        }

        ui.btnCancel.setOnClickListener { dismiss() }

        ui.btnSubmit.setOnClickListener {
            requestPost()
        }

        //
        vm.loading.observe(this) {
            ui.btnSubmit.isEnabled = it == 0
            ui.progress.isVisible = it > 0
        }
    }

    private fun setupPager() {
        val pages = arrayOf(
            "Propostas" to { DialogDriverAdProposal() },
            "Rota" to { DialogDriverAdProposalRoute() },
            "Positivo" to { FragNone() },
            "Negativo" to { FragNone() }
        )
        val adapter = object : FragmentStateAdapter(childFragmentManager, lifecycle) {
            override fun getItemCount() = pages.size
            override fun createFragment(position: Int) = pages[position].second()
        }
        ui.pager.adapter = adapter
        val tabLayoutMediator = TabLayoutMediator(ui.tabLayout, ui.pager, true) { tab, position ->
            tab.text = pages[position].first
        }
        tabLayoutMediator.attach()
    }

    private fun requestPost()
    {
        vm.incLoading(1)
        val params = HttpFormData().put("price", vm.price.value!!).put("arrival", vm.arrival)
        Http.post("ad/${cModel.id}/proposal", params)
            .then {
                val proposal = JProposal.parsePayload(it)
                DialogDriverAdProposal.shared?.outUpdateSingle(proposal)

            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.incLoading(-1)
            }
    }

    override fun onStart() {
        super.onStart()
        shared = this
    }

    override fun onStop() {
        super.onStop()
        shared = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

    //
    interface Listener {
        fun dialogDriverAdRequest(): JAd
    }
}