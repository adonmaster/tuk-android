package com.adon.tuk.dialogs.cityPicker

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.RvData
import com.adon.tuk.helpers.ter
import com.adon.tuk.models.City
import com.adon.tuk.repo.Repo

class RVAdapter(private val listener: Listener): RecyclerView.Adapter<RVAdapter.VH>() {

    //
    private var _data = RvData<City, RVAdapter.VH>(this)
    private val data get() = _data.items()

    //
    fun query(s: String) {
        val list = Repo.city.query(s)
        _data.update(list)
    }

    //
    override fun getItemCount() = data.count()

    override fun getItemId(position: Int) = data[position].id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_city_picker, parent, false)!!
        return VH(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = data[position]
        holder.title.text = "${m.name} - ${m.uf}"
        holder.title.setTypeface(null, ter(m.star > 1, Typeface.BOLD, Typeface.NORMAL))
    }

    //
    interface Listener {
        fun onSelected(item: City)
    }

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView = itemView.findViewById(R.id.lbl_title)

        init {
            itemView.setOnClickListener {
                listener.onSelected(data[absoluteAdapterPosition])
            }
        }
    }

}