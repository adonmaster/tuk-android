package com.adon.tuk.dialogs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.jmodels.JAd

class DialogDriverAdVM: ViewModel() {

    var ad: JAd? = null
    val price = MutableLiveData(0.0)
    var arrival = 5

    val loading = MutableLiveData(0)
    fun incLoading(factor: Int) {
        loading.value = loading.value!! + factor
    }

}