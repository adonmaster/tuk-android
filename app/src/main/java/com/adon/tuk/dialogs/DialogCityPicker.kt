package com.adon.tuk.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.SearchView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.databinding.DialogCityPickerBinding
import com.adon.tuk.dialogs.cityPicker.RVAdapter
import com.adon.tuk.helpers.Task
import com.adon.tuk.models.City

class DialogCityPicker: DialogBase() {

    companion object {
        fun show(fm: FragmentManager?) {
            if (fm == null) return
            val dialog = DialogCityPicker()
            dialog.isCancelable = false

            dialog.show(fm, "DialogCityPicker")
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(l: Listener) { listeners.add(l) }
        fun detach(l: Listener) { listeners.remove(l) }
    }

    //
    private var _ui: DialogCityPickerBinding? = null
    private val ui get() = _ui!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogCityPickerBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        ui.rv.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        ui.rv.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        ui.rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down)
        val adapter = RVAdapter(object : RVAdapter.Listener {
            override fun onSelected(item: City) {
                Task.main(250) {
                    dismiss()
                    listeners.forEach { it.dialogCityPickerOnSelect(item) }
                }
            }
        })
        ui.rv.adapter = adapter
        adapter.query("")

        ui.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean { return true }
            override fun onQueryTextChange(p0: String?): Boolean {
                Task.debounce("DialogCityPicker@search:query", 800) {
                    adapter.query(p0 ?: "")
                    ui.rv.scrollToPosition(0)
                }
                return true
            }
        })

        //
        ui.btnCancel.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

    //
    interface Listener {
        fun dialogCityPickerOnSelect(city: City)
    }

}