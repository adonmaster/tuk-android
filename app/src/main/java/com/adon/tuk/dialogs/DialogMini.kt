package com.adon.tuk.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.adon.tuk.R
import com.adon.tuk.databinding.DialogMiniBinding
import com.adon.tuk.helpers.Cl

class DialogMini: DialogBase() {

    companion object {
        private fun show(
            frag: Fragment?, icon: String, title: String, body: String,
            @ColorRes color: Int, tag: String=""
        )
        {
            val fm = frag?.childFragmentManager ?: return

            val dialog = DialogMini()
            dialog.arguments = bundleOf(
                "icon" to icon, "title" to title, "body" to body,
                "tag" to tag, "color" to Cl.res(color)
            )
            dialog.show(fm, "DialogMini")
        }
        fun success(frag: Fragment?, msg: String, title: String="Ok", tag: String="") {
            show(frag, "\uF058", title, msg, R.color.md_green_500, tag)
        }
        fun error(frag: Fragment?, msg: String, title: String="Erro", tag: String="") {
            show(frag, "\uF06A", title, msg, R.color.md_red_500, tag)
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(l: Listener) { listeners.add(l) }
        fun detach(l: Listener) { listeners.remove(l) }
    }

    //
    private var _ui: DialogMiniBinding? = null
    private val ui get() = _ui!!

    private val cIcon get() = requireArguments().getString("icon")!!
    private val cTitle get() = requireArguments().getString("title")!!
    private val cBody get() = requireArguments().getString("body")!!
    private val cColor get() = requireArguments().getInt("color")
    private val cTag get() = requireArguments().getString("tag")!!


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogMiniBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui.lblIcon.text = cIcon
        ui.lblIcon.setTextColor(cColor)

        ui.lblTitle.text = cTitle

        ui.lblBody.text = cBody
        ui.lblBody.setTextColor(cColor)

        ui.lblClose.setOnClickListener {
            dialog?.cancel()
        }
    }

    //
    interface Listener {

    }

}