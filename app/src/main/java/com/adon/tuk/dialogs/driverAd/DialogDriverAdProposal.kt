package com.adon.tuk.dialogs.driverAd

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.databinding.DialogDriverAdProposalBinding
import com.adon.tuk.dialogs.DialogDriverAdVM
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.helpers.Http
import com.adon.tuk.jmodels.JProposal

class DialogDriverAdProposal: Fragment() {

    companion object {
        var shared: DialogDriverAdProposal? = null
    }

    //
    private val vm by lazy { ViewModelProvider(requireActivity())[DialogDriverAdVM::class.java] }
    private var _ui: DialogDriverAdProposalBinding? = null
    private val ui get() = _ui!!

    //
    private lateinit var mAdapter: DialogDriverAdProposalAdapter

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogDriverAdProposalBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter = DialogDriverAdProposalAdapter(object : DialogDriverAdProposalAdapter.Listener {
            override fun onSelected(model: JProposal, position: Int) {
            }
            override fun onEmpty(option: Boolean) {
                ui.wrapperEmpty.isVisible = option
            }
        })
        ui.rv.adapter = mAdapter
        ui.rv.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        ui.rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down)

        vm.loading.observe(viewLifecycleOwner) {
            ui.progress.isVisible = it > 0
        }
    }

    private fun request()
    {
        vm.incLoading(1)
        Http.getString("ad/${vm.ad!!.id}/proposal")
            .then {
                mAdapter.update(JProposal.parseList(it))
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.incLoading(-1)
            }
    }

    fun outRequestProposal(ad_id: Int) {
        if (ad_id == vm.ad!!.id.toInt()) request()
    }

    fun outUpdateSingle(proposal: JProposal) {
        mAdapter.updateSingle(proposal)
    }

    override fun onStart() {
        super.onStart()
        shared = this
        request()
    }

    override fun onStop() {
        super.onStop()
        shared = null
    }

}