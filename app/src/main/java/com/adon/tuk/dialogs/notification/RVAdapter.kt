package com.adon.tuk.dialogs.notification

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.MainActivity
import com.adon.tuk.R
import com.adon.tuk.helpers.*
import com.adon.tuk.models.NotificationModel
import com.adon.tuk.repo.Repo

class RVAdapter(private val listener: Listener): RecyclerView.Adapter<RVAdapter.VH>()
{
    private var _data = RvData<NotificationModel, RVAdapter.VH>(this)
    private val data get() = _data.items()

    private var seenStashIds = arrayListOf<Long>()
    fun seen(ini: Int, end: Int) {
        for (index in ini..end) {
            val m = data.getOrNull(index)
            if (m != null && ! m.seen) {
                m.seen = true
                seenStashIds.add(m.id)
                Task.debounce("DialogNotification:RVAdapter@seen", 1700) {
                    val ids = seenStashIds.toList()
                    seenStashIds.clear()

                    Repo.notification.seen(ids)
                    MainActivity.shared?.outUpdateNotificationBadge()
                }
            }
        }
    }

    fun updateData(items: List<NotificationModel>) {
        _data.update(items)
    }

    override fun getItemCount(): Int {
        val n = data.count()
        listener.onEmpty(n == 0)
        return n
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVAdapter.VH {
        val v = Act.inflate(R.layout.rv_dialog_notification, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: RVAdapter.VH, position: Int)
    {
        val m = data[position]

        holder.itemView.setBackgroundColor(Cl.res(ter(m.seen, R.color.transparent, R.color.md_yellow_50)))
        holder.title.text = m.msg
        holder.timestamp.text = Dt.fromNow(m.created_at)
        holder.icon.text = m.p.icon()
    }

    //
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val title: TextView = itemView.findViewById(R.id.lbl_title)
        val timestamp: TextView = itemView.findViewById(R.id.lbl_timestamp)
        val icon: TextView = itemView.findViewById(R.id.lbl_left_icon)

        init {
            itemView.setOnClickListener {
                // absoluteAdapterPosition
            }
        }

    }

    //
    interface Listener {
        fun onEmpty(option: Boolean)
    }

}