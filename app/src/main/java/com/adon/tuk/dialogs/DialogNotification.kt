package com.adon.tuk.dialogs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.App
import com.adon.tuk.databinding.DialogNotificationBinding
import com.adon.tuk.dialogs.notification.RVAdapter
import com.adon.tuk.helpers.Rv
import com.adon.tuk.helpers.Task
import com.adon.tuk.repo.Repo

class DialogNotification: DialogBase() {

    companion object {
        fun show(fm: FragmentManager?) {
            if (fm == null) return
            val dialog = DialogNotification()
            dialog.show(fm, "DialogNotification")
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(l: Listener) { listeners.add(l) }
        fun detach(l: Listener) { listeners.remove(l) }
    }

    //
    private var _ui: DialogNotificationBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogNotificationBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        Rv.setupForTable(ui.rv, requireContext(), true)
        val adapter = RVAdapter(object : RVAdapter.Listener {
            override fun onEmpty(option: Boolean) {
                ui.wrapperEmpty.isVisible = option
            }
        })
        ui.rv.adapter = adapter
        ui.rv.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val posIni = layoutManager.findFirstVisibleItemPosition()
                val posEnd = layoutManager.findLastVisibleItemPosition()
                adapter.seen(posIni, posEnd)
            }
        })
        adapter.updateData(Repo.notification.getAll(App.userId))

        //
        ui.btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        ui.btnClear.setOnClickListener {
            Repo.notification.deleteAll(App.userId)
            adapter.updateData(listOf())
        }
    }

    //
    interface Listener {

    }

}