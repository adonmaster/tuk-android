package com.adon.tuk.dialogs.adHire

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.DialogAdDriverHireCommentsBinding

class DialogAdDriverHireComments: Fragment() {

    companion object {
        fun f(positive: Boolean): DialogAdDriverHireComments {
            val f = DialogAdDriverHireComments()
            f.arguments = bundleOf("positive" to positive)
            return f
        }
    }

    //
    private var _ui: DialogAdDriverHireCommentsBinding? = null
    private val ui get() = _ui!!

    //
    private val cPositive = arguments?.getBoolean("positive") ?: false

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogAdDriverHireCommentsBinding.inflate(inflater, container, false)
        return ui.root
    }

}