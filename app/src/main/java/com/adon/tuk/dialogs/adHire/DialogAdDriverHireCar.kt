package com.adon.tuk.dialogs.adHire

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.DialogAdDriverHireCarBinding

class DialogAdDriverHireCar: Fragment() {

    //
    private var _ui: DialogAdDriverHireCarBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = DialogAdDriverHireCarBinding.inflate(inflater, container, false)
        return ui.root
    }

}