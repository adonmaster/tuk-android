package com.adon.tuk.jmodels

import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.JParser
import com.adon.tuk.structs.GeoPath
import com.google.android.gms.maps.model.LatLng
import org.json.JSONArray
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class JPostAd(
    val payload: JAd
) {
    companion object {
        fun parse(r: JSONObject): JPostAd {
            val json = r.getJSONObject("payload")
            return JPostAd(JAd.parse(json))
        }
    }
}