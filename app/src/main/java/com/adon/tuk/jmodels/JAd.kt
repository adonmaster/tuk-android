package com.adon.tuk.jmodels

import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.JParser
import com.adon.tuk.helpers.Str
import com.adon.tuk.structs.GeoPath
import com.adon.tuk.structs.Idable
import com.google.android.gms.maps.model.LatLng
import org.json.JSONArray
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class JSimpleUser(
    val id: Long,
    val name: String,
    val avatar_thumb: String?,
    val avatar_full: String?
)

data class JAd(
    val id: Long,
    val user_id: Int,
    val user_name: String,
    val origin_lat: Double,
    val origin_lng: Double,
    val origin_s: String,
    val destination_lat: Double,
    val destination_lng: Double,
    val destination_s: String,
    val route: List<List<LatLng>>?,
    val duration: Int,
    val duration_s: String,
    val distance: Int,
    val distance_s: String,
    val price: Double,
    val created_at: LocalDateTime,
    val user: JSimpleUser?
): Idable {

    override fun idable() = id

    companion object {

        fun parse(s: String?): JAd? {
            if (s == null) return null
            return parse(JSONObject(s))
        }

        fun parseList(s: String?): List<JAd> {
            val r = arrayListOf<JAd>()
            val jlist = JSONArray(Str.coalesce(s, "[]"))
            for (i in 0 until jlist.length()) {
                r.add(parse(jlist.getJSONObject(i)))
            }
            return r
        }
        fun parse(json: JSONObject) = JParser(json) {
            store("id")
            store("user_id")
            store("user_name")
            store("origin_lat")
            store("origin_lng")
            store("origin_s")
            store("destination_lat")
            store("destination_lng")
            store("destination_s")
            store("route")
            store("duration")
            store("duration_s")
            store("distance")
            store("distance_s")
            store("price")
            store("created_at")
            objo("user") {
                store("id")
                store("name")
                store("avatar_thumb")
                store("avatar_full")
            }
        }.parse {
            JAd(
                getNext(), getNext(), getNext(), getNext(), getNext(), getNext(), getNext(), getNext(),
                getNext(),
                GeoPath.from(getNexto<JSONArray>()),
                getNext(), getNext(), getNext(), getNext(), getNext(),
                Dt.fromUtcIsoJson(getNext())!!,
                objo("user") {
                    JSimpleUser(getNext(), getNext(), getNexto(), getNexto())
                }
            )
        }
    }

    fun origin(): LatLng {
        return LatLng(origin_lat, origin_lng)
    }

    fun destination() : LatLng {
        return LatLng(destination_lat, destination_lng)
    }
}