package com.adon.tuk.jmodels

import com.adon.tuk.helpers.JParser
import org.json.JSONObject

data class JMedia(val full: String, val thumb: String, val uid: String) {

    companion object {
        fun parse(r: JSONObject): JMedia = JParser(r) {
            obj("payload") {
                store("url_full")
                store("url_thumb")
                store("uid")
            }
        }.parse {
            obj("payload") {
                JMedia(getNext(), getNext(), getNext())
            }
        }
    }

}

/**
{
"message": "Ok!",
"payload": {
"id": 3,
"uid": "c57602c5cbcb8466619291a2c855bc",
"type": "image",
"disk": "aws:s3:tuk-media",
"path_full": "1db2d4875f7a3e297014efb31ffe30a5-cropped5727012909560756660.jpg",
"path_thumb": "thumb_1db2d4875f7a3e297014efb31ffe30a5-cropped5727012909560756660.jpg",
"url_full": "https://d2ttsvprgfygt9.cloudfront.net/1db2d4875f7a3e297014efb31ffe30a5-cropped5727012909560756660.jpg",
"url_thumb": "https://d2ttsvprgfygt9.cloudfront.net/thumb_1db2d4875f7a3e297014efb31ffe30a5-cropped5727012909560756660.jpg",
"updated_at": "2022-02-21T18:06:47.291Z",
"created_at": "2022-02-21T18:06:47.291Z"
}
}

 */