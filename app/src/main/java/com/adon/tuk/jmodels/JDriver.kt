package com.adon.tuk.jmodels

import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.JParser
import org.json.JSONObject
import org.threeten.bp.LocalDateTime


class JDriver(
    val id: Int,
    val user_id: Int,
    val name_first: String,
    val name_last: String,
    val cpf: String,
    val ref_city: String,
    val ref_uf: String,
    val avatar: String?,
    val avatar_thumb: String?,
    val avatar_uid: String?,
    val pos_lat: Double,
    val pos_lng: Double,
    val car_id: Int,
    val active: Boolean,
    var online: Boolean,
    val points: Int,
    val created_at: LocalDateTime,
    val updated_at: LocalDateTime
) {
    companion object {
        fun parse(s: String?): JDriver? {
            if (s?.isNotEmpty() == true) {
                return parse(JSONObject(s))
            }
            return null
        }
        fun parse(json: JSONObject) = JParser(json) {
            store("id")
            store("user_id")
            store("name_first")
            store("name_last")
            store("cpf")
            store("ref_city")
            store("ref_uf")
            store("avatar")
            store("avatar_thumb")
            store("avatar_uid")
            store("pos_lat")
            store("pos_lng")
            store("car_id")
            store("active")
            store("online")
            store("points")
            store("created_at")
            store("updated_at")
        }.parse {
            JDriver(
                getNext(), getNext(), getNext(), getNext(), getNext(), getNext(), getNext(),
                getNexto(), getNexto(), getNexto(), getNext(), getNext(), getNext(), getNext(),
                getNext(), getNext(),
                Dt.fromUtcIsoJson(getNext())!!,
                Dt.fromUtcIsoJson(getNext())!!
            )
        }
    }

    fun toJSON(): String {
        val r = JSONObject()
        r.put("id", id)
        r.put("user_id", user_id)
        r.put("name_first", name_first)
        r.put("name_last", name_last)
        r.put("cpf", cpf)
        r.put("ref_city", ref_city)
        r.put("ref_uf", ref_uf)
        r.put("avatar", avatar)
        r.put("avatar_thumb", avatar_thumb)
        r.put("avatar_uid", avatar_uid)
        r.put("pos_lat", pos_lat)
        r.put("pos_lng", pos_lng)
        r.put("car_id", car_id)
        r.put("active", active)
        r.put("online", online)
        r.put("points", points)
        r.put("created_at", Dt.toUtcIso(created_at))
        r.put("updated_at", Dt.toUtcIso(updated_at))
        return r.toString()
    }
}