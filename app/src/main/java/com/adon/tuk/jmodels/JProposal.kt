package com.adon.tuk.jmodels

import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.JParser
import com.adon.tuk.helpers.Str
import com.adon.tuk.structs.Idable
import org.json.JSONArray
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class JProposal(
    val id: Long,
    val ad_id: Int,
    val ad_user_id: Int,
    val driver_id: Int,
    val driver_user_id: Int,
    val driver_name_first: String,
    val driver_name_last: String,
    val driver_pos_lat: Double,
    val driver_pos_lng: Double,
    val driver_avatar_thumb: String?,
    val driver_car: String,
    val price: Double,
    val arrival: Int,
    val updated_at: LocalDateTime,
    val created_at: LocalDateTime,
): Idable {

    companion object {

        fun parse(j: JSONObject) = JParser(j) {
            store("id")
            store("ad_id")
            store("ad_user_id")
            store("driver_id")
            store("driver_user_id")
            store("driver_name_first")
            store("driver_name_last")
            store("driver_pos_lat")
            store("driver_pos_lng")
            store("driver_avatar_thumb")
            store("driver_car")
            store("price")
            store("arrival")
            store("updated_at")
            store("created_at")
        }.parse {
            JProposal(
                getNext(), getNext(), getNext(), getNext(), getNext(), getNext(), getNext(), getNext(),
                getNext(), getNexto(), getNext(), getNext(), getNext(),
                Dt.fromUtcIsoJson(getNext())!!, Dt.fromUtcIsoJson(getNext())!!
            )
        }

        fun parseList(s: String?): List<JProposal> {
            val r = arrayListOf<JProposal>()
            val jlist = JSONArray(Str.coalesce(s, "[]"))
            for (i in 0 until jlist.length()) {
                r.add(parse(jlist.getJSONObject(i)))
            }
            return r
        }

        fun parsePayload(json: JSONObject) = parse(json.getJSONObject("payload"))

    }

    override fun idable(): Long {
        return id
    }

}