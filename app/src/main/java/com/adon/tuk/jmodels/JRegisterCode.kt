package com.adon.tuk.jmodels

import com.adon.tuk.helpers.JParser
import org.json.JSONObject

data class JRegisterCode(
    val id: Long, val email: String, val token: String, val name: String, val avatar: String?, val avatar_thumb: String?
)
{
    companion object {
        fun parse(r: JSONObject): JRegisterCode = JParser(r) {
            obj("payload") {
                store("id")
                store("email")
                store("token")
                store("name")
                store("avatar")
                store("avatar_thumb")
            }
        }.parse {
            obj("payload") {
                JRegisterCode(getNext(), getNext(), getNext(), getNext(), getNexto(), getNexto())
            }
        }
    }

}