package com.adon.tuk.seeders

import android.content.Context
import android.util.Log
import com.adon.tuk.R
import com.adon.tuk.db.DB
import com.adon.tuk.helpers.Str
import com.adon.tuk.helpers.jex
import com.adon.tuk.models.City
import com.adon.tuk.repo.Repo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CitySeeder(private val context: Context) {

    fun process() {
        CoroutineScope(IO).launch {

            //DB.i.city.deleteAll()
            if (Repo.city.hasAny()) return@launch

            val i = context.resources.openRawResource(R.raw.estados_cidades)
            val reader = i.bufferedReader()
            val text = reader.use { it.readText() }
            kotlin.runCatching { reader.close() }

            val cities = mutableListOf<City>()

            val star5 = listOf(
                "Goiânia", "Aparecida de Goiânia", "Anápolis", "Trindade",
                "São Paulo", "Rio de Janeiro"
            )

            jex(text) {
                arrs("estados") {
                    val uf = gStr("sigla")
                    val state =  gStr("nome")
                    arrStrs("cidades") { name ->
                        val model = City()
                        model.name = name
                        model.name_query = Str.generateQueryStr(name)
                        model.state = state
                        model.uf = uf
                        model.star = when(true) {
                            star5.contains(name) -> 5
                            else -> 0
                        }

                        cities.add(model)
                    }
                }
            }

            Repo.city.seed(cities)
        }
    }

}