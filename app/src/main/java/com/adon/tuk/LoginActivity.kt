package com.adon.tuk

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.adon.tuk.helpers.Task

class LoginActivity : AppCompatActivity() {

    // static
    companion object {
        fun start(activity: Activity) {
            Task.debounce("LoginActivity@static:start", 1000) {
                val intent = Intent(activity, LoginActivity::class.java)
                activity.startActivityForResult(intent, 551)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

}