package com.adon.tuk

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.helpers.ter

class DriverRequestVM: ViewModel() {

    //
    val loading = MutableLiveData(false)

    //
    val detail = HashMap<String, String?>()
    val detailAppendTitle = MutableLiveData("")
    val detailDone = MutableLiveData(false)
    fun detailPut(key: String, content: String?) {
        put(detail, key, content, 10, detailAppendTitle, detailDone)
    }

    //
    val license = HashMap<String, String?>()
    val licenseAppendTitle = MutableLiveData("")
    val licenseDone = MutableLiveData(false)
    fun licensePut(key: String, content: String?) {
        put(license, key, content, 4, licenseAppendTitle, licenseDone)
    }

    //
    val legal = HashMap<String, String?>()
    val legalAppendTitle = MutableLiveData("")
    val legalDone = MutableLiveData(false)
    fun legalPut(key: String, content: String?) {
        put(legal, key, content, 3, legalAppendTitle, legalDone)
    }

    //
    val car = HashMap<String, String?>()
    val carAppendTitle = MutableLiveData("")
    val carDone = MutableLiveData(false)
    fun carPut(key: String, content: String?) {
        put(car, key, content, 8, carAppendTitle, carDone)
    }

    //
    private fun put(
        hash: HashMap<String, String?>,
        key: String, content: String?,
        size: Int,
        title: MutableLiveData<String>,
        done: MutableLiveData<Boolean>
    ) {
        hash[key] = content

        val cont = hash.values.mapNotNull { it?.takeIf { it.isNotBlank() } }.size
        title.value = " $cont/$size"

        done.value = cont == size
    }
}