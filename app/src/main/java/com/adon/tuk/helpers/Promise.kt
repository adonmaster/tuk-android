package com.adon.tuk.helpers

class Promise<V>(private val cb: ((V) -> Unit, (String) -> Unit) -> Unit) {

    private var resolver: (V)->Unit = {}
    private var rejector: (String)->Unit = {}
    private var doner: (()->Unit)? = null

    init {
        start()
    }

    private fun start() {
        Task.main {
            try {
                cb(resolver, rejector)
            } catch (e: Exception) {
                rejector(e.localizedMessage ?: "Erro insano 442")
            }
        }
    }

    fun then(cb: (V)->Unit): Promise<V> {
        resolver = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun catch(cb: (String)->Unit): Promise<V> {
        rejector = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun always(cb: ()->Unit) {
        doner = cb
    }

}

fun <T> promiseIt(obj: T?) = Promise<T> { resolve, reject ->
    try {
        if (obj!=null) {
            resolve(obj)
        } else {
            reject("objeto vazio")
        }
    } catch(e: Exception) {
        reject(e.localizedMessage ?: "Erro insano 441")
    }
}
