package com.adon.tuk.helpers

class LightCache(private val capacity: Int) {

    private val cache = arrayListOf<Pair<String, Any?>>()

    @Suppress("UNCHECKED_CAST")
    fun <T> get(key: String, cb: ()->T?, forceRefresh: Boolean=false): T?
    {
        val index = cache.indexOfFirst { it.first == key }
        if ( ! forceRefresh && index >= 0) return cache[index].second as T

        val v = cb()
        if (index >= 0) {
            cache[index] = key to v
        } else {
            appendCache(key, v)
        }
        return v
    }

    private fun appendCache(key: String, v: Any?) {
        cache.add(key to v)
        if (cache.size > capacity) cache.removeAt(0)
    }

    fun <T> getPromise(key: String, promise: ()->Promise<T>) = Promise<T> { resolve, reject ->

        val index = cache.indexOfFirst { it.first == key }
        if (index >= 0) {
            resolve(cache[index].second as T)
        } else {
            promise().then { v ->
                appendCache(key, v)
                resolve(v)
            }.catch(reject)
        }
    }

    fun <T> getGranted(key: String, cb: ()->T, forceRefresh: Boolean=false): T {
        return get(key, cb, forceRefresh)!!
    }

    fun remove(key: String) {
        val iterator = cache.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().first == key) iterator.remove()
        }
    }

    fun clear() {
        cache.clear()
    }

}