package com.adon.tuk.helpers

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

class FlowEx<V>(private var _value: V?) {

    private var owners = HashMap<LifecycleOwner, (V?)->Unit>()

    val v get() = _value!!
    val value get() = _value

    fun observe(owner: LifecycleOwner, cb: (V?)->Unit) {
        owners[owner] = cb
    }

    fun emit(value: V?)
    {
        this._value = value

        val iterator = owners.iterator()
        while (iterator.hasNext()) {
            val o = iterator.next()
            if (o.key.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                o.value.invoke(value)
            } else {
                iterator.remove()
            }
        }
    }

    fun toObserver() = Observer(this)

    class Observer<T>(private val flow: FlowEx<T>) {
        fun observe(owner: LifecycleOwner, cb: (T?)->Unit) = flow.observe(owner, cb)
    }

}