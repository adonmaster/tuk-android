package com.adon.tuk.helpers

class ResultExO<V> private constructor(val data: V?, val error: String?, val isSuccess: Boolean) {

    companion object {
        fun <V> success(data: V?): ResultExO<V> {
            return ResultExO(data, null, true)
        }
        fun <V> fail(reason: String): ResultExO<V> {
            return ResultExO(null, reason, false)
        }
    }

    inline fun then(cb: (V?)->Unit): ResultExO<V> {
        if (isSuccess) cb(data)
        return this
    }

    inline fun catch(cb: (String)->Unit): ResultExO<V> {
        if (!isSuccess) cb(error!!)
        return this
    }
}

class ResultEx<V> private constructor(val data: V?, val error: String?, val isSuccess: Boolean) {

    companion object {
        fun <V> success(data: V): ResultEx<V> {
            return ResultEx(data, null, true)
        }
        fun <V> fail(reason: String): ResultEx<V> {
            return ResultEx(null, reason, false)
        }
    }

    inline fun then(cb: (V)->Unit): ResultEx<V> {
        if (isSuccess) cb(data!!)
        return this
    }

    inline fun catch(cb: (String)->Unit): ResultEx<V> {
        if (!isSuccess) cb(error!!)
        return this
    }
}

