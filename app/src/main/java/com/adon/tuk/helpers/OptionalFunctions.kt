package com.adon.tuk.helpers

inline fun <T> guard(obj: T?, bye: ()->Unit): T {
    if (obj==null) {
        bye()
        throw IllegalArgumentException("Use return with guard, damn it!")
    }
    return obj
}

inline fun <T> ter(cb: ()->Boolean, yep: T, nop: T): T {
    return ter(cb(), yep, nop)
}

fun <T> ter(condition: Boolean, yep: T, nop: T): T {
    return if (condition) yep else nop
}

inline fun <T> ter(condition: Boolean, cbYep: ()->T, cbNop: ()->T): T {
    if (condition) return cbYep()
    return cbNop()
}


fun <V> quad(vararg pairs: Pair<V, Boolean>): V? {
    for (pair in pairs) {
        if (pair.second) return pair.first
    }
    return null
}

fun <V> coalesce(vararg list: V?): V {
    return coalesce(*list.map { { it } }.toTypedArray())
}

fun <V> coalesce(vararg list: ()->V?): V {
    var v: V? = null
    for (i in list) {
        v = i()
        if (v!=null) return v
    }
    return v!!
}

fun <V> grant(vararg list: V?): Boolean {
    for (i in list) if (i == null) return false
    return true
}