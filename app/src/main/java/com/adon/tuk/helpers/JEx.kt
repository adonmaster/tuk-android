package com.adon.tuk.helpers

import org.json.JSONObject

/**
 * @JEx version 2.0
 */
fun jex(json: String?, block: JExDsl.()->Unit): JExResult {
    if (json!=null) {
        try {
            val jsonObject = JSONObject(json)
            return jex(jsonObject, block)
        } catch (e: Throwable) {
            JExResult(JExDsl(null), e)
        }
    }
    return JExResult(JExDsl(null), Throwable("Vazio"))
}

fun jex(json: JSONObject?, block: JExDsl.()->Unit): JExResult {
    val jExInstance = JExDsl(json)
    try {
        block(jExInstance)
    } catch (e: Throwable) {
        return JExResult(jExInstance, e)
    }
    return JExResult(jExInstance)
}

open class JExDsl(private val json: JSONObject?) {

    val keys: List<String>
        get() = json?.keys()?.asSequence()?.toList() ?: listOf()

    fun o(field: String, block: JExDsl.()->Unit) {
        val j = if (json!!.isNull(field)) null else json.getJSONObject(field)
        jex(j, block)
    }

    fun o(field: String): JSONObject? {
        return if (json!!.isNull(field)) null else json.getJSONObject(field)
    }

    fun oGuarded(field: String, block: JExDsl.()->Unit) {
        val j = if (json!!.isNull(field)) null else json.getJSONObject(field)
        if (j!=null) jex(j, block)
    }

    fun str(field: String, default: String?=null): String? {
        if (json!!.isNotNull(field)) return json.getString(field)
        return default
    }

    fun gStr(field: String, default: String=""): String {
        return str(field, default)!!
    }

    fun bool(field: String): Boolean {
        return json?.getBoolean(field) ?: false
    }

    fun arr(field: String, block: JExDsl.()->Boolean) {
        val array = json!!.getJSONArray(field)
        for (index in 0 until array.length()) {
            var passed = true
            jex(array.getJSONObject(index)) {
                passed = block(this)
            }
            if ( ! passed) break
        }
    }

    fun arrs(field: String, block: JExDsl.()->Unit) {
        val array = json!!.getJSONArray(field)
        for (index in 0 until array.length()) {
            block(JExDsl(array.getJSONObject(index)))
        }
    }

    fun arrStr(field: String, block: (String)->Boolean) {
        val array = json!!.getJSONArray(field)
        for (index in 0 until array.length()) {
            if ( ! block(array.getString(index))) break
        }
    }

    fun arrStrs(field: String, block: (String)->Unit) {
        arrStr(field) {
            block(it)
            true
        }
    }

    fun long(field: String, default: Long?=null): Long? {
        if (json!!.isNotNull(field)) return json.getLong(field)
        return default
    }

    fun i(field: String, default: Int?): Int? {
        if (json!!.isNotNull(field)) return json.getInt(field)
        return default
    }

    fun gi(field: String): Int {
        return json!!.getInt(field)
    }

    fun gLong(field: String, default: Long=0): Long {
        return long(field, default)!!
    }

    fun b(field: String): Boolean {
        return json!!.getBoolean(field)
    }

    fun d(field: String, default: Double?=null): Double? {
        if (json!!.isNotNull(field)) return json.getDouble(field)
        return default
    }

    fun gd(field: String, default: Double=0.0): Double {
        return d(field, default)!!
    }

    fun getObject() = json
}

class JExResult(private val j: JExDsl, private val error: Throwable?=null) {

    fun onError(cb: (String)->Unit): JExResult {
        if (error!=null) cb(error.localizedMessage ?: "Erro insano 4421")
        return this
    }

}

fun JSONObject.isNotNull(name: String): Boolean {
    return !isNull(name)
}