package com.adon.tuk.helpers

import android.content.Context
import android.graphics.Color
import android.location.Geocoder
import com.adon.tuk.structs.GeoDistance
import com.adon.tuk.structs.JGeoFull
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import org.json.JSONObject
import java.util.*

object Geo {

    suspend fun addressLineFrom(context: Context, coord: LatLng): String {
        val geo = Geocoder(context, Locale.getDefault())
        val address = geo.getFromLocation(coord.latitude, coord.longitude, 3).lastOrNull()
        return address?.getAddressLine(0) ?: "Endereço..."
    }

    fun requestRoute(ori: LatLng?, dest: LatLng?): Promise<List<List<LatLng>>>?
    {
        val origin = ori ?: return null
        val destination = dest ?: return null

        val params = HttpFormData()
            .put("origin", "${origin.latitude},${origin.longitude}")
            .put("destination", "${destination.latitude},${destination.longitude}")

        return Promise { resolve, reject ->
            Http.post("geo/route", params)
                .then { json ->
                    val j = json.getJSONObject("payload")
                    if (j.getString("status") != "OK") {
                        var msg = "Erro ao buscar rota"
                        if (j.has("message")) msg = j.getString("message")
                        reject(msg)
                        return@then
                    }

                    val path: MutableList<List<LatLng>> = ArrayList()
                    val routes = j.getJSONArray("routes")
                    val legs = routes.getJSONObject(0).getJSONArray("legs")
                    val steps = legs.getJSONObject(0).getJSONArray("steps")
                    for (i in 0 until steps.length()) {
                        val points = steps
                            .getJSONObject(i)
                            .getJSONObject("polyline")
                            .getString("points")
                        path.add(PolyUtil.decode(points))
                    }

                    resolve(path)
                }
                .catch(reject)
        }

    }

    fun routeLoadPath(g: GoogleMap, path: List<List<LatLng>>) {
        for (element in path) {
            g.addPolyline(PolylineOptions().addAll(element).color(Color.BLACK))
        }
    }

    fun requestDistance(ori: LatLng?, dest: LatLng?): Promise<GeoDistance>?
    {
        val origin = ori ?: return null
        val destination = dest ?: return null

        val params = HttpFormData()
            .put("origin", "${origin.latitude},${origin.longitude}")
            .put("destination", "${destination.latitude},${destination.longitude}")

        return Promise { resolve, reject ->

            Http.post("geo/distance", params)
                .then {
                    val j = it.getJSONObject("payload")
                    if (j.getString("status") != "OK") {
                        var msg = "Erro ao buscar rota"
                        if (j.has("message")) msg = j.getString("message")
                        reject(msg)
                        return@then
                    }

                    val row = j.getJSONArray("rows").getJSONObject(0)
                    val element = row.getJSONArray("elements").getJSONObject(0)

                    val destinationAddr = j.getJSONArray("destination_addresses").getString(0)
                    val originAddr = j.getJSONArray("origin_addresses").getString(0)

                    val distanceStr = element.getJSONObject("distance").getString("text")
                    val distance = element.getJSONObject("distance").getInt("value")

                    val durationStr = element.getJSONObject("duration").getString("text")
                    val duration = element.getJSONObject("duration").getInt("value")

                    resolve(GeoDistance(duration, durationStr, distance, distanceStr, originAddr, destinationAddr))
                }
                .catch(reject)
        }

    }

    fun requestFull(origin: LatLng, destination: LatLng): Promise<JGeoFull>
    {
        val params = HttpFormData()
            .put("origin", "${origin.latitude},${origin.longitude}")
            .put("destination", "${destination.latitude},${destination.longitude}")

        return Promise { resolve, reject ->

            Http.post("geo/full", params)
                .then { json ->
                    jex(json) {
                        o("payload") {
                            val route = o("route")!!
                            val distance = o("distance")!!
                            JGeoFull.parse(route, distance).then(resolve).catch(reject)
                        }
                    }
                }
                .catch(reject)
        }

    }

}