package com.adon.tuk.helpers

import android.annotation.SuppressLint
import android.content.Context
import com.adon.tuk.R
import java.text.NumberFormat
import java.util.*
import kotlin.random.Random

object Str {

    private const val mCollectionAlphaNumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
    private var mRandom = Random(23451)

    fun random(len: Int): String {
        val sb = StringBuilder(len)
        sb.append(Calendar.getInstance().time.time)
        for (i in 0 until len-sb.count()) {
            sb.append(
                mCollectionAlphaNumeric[mRandom.nextInt(
                    mCollectionAlphaNumeric.length)])
        }
        return sb.toString()
    }

    fun coalesce(vararg args: String?): String {
        for (arg in args) {
            if (arg?.isNullOrBlank() == false) return arg
        }
        return ""
    }

    fun colorEmpty(context: Context, s: String?, emptyText: String): CharSequence? {
        return if (s?.trim()?.isEmpty() == false) s else Span(
            emptyText
        )
            .color(context, R.color.md_grey_700)
            .build()
    }

    fun upper(s: String?): String? {
        return s?.uppercase(Locale.getDefault())
    }

    @SuppressLint("DefaultLocale")
    fun containsInsensitive(needle: String, vararg haystack: String): Boolean {
        for (cur in haystack) {
            if (needle.trim().lowercase(Locale.getDefault()) == cur.trim()
                    .lowercase(Locale.getDefault())) {
                return true
            }
        }
        return false
    }

    fun extractYTIdFrom(url: String?): String? {
        if (url==null) return null
        val regex = """(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/user/\S+|/ytscreeningroom\?v=|/sandalsResorts#\w/\w/.*/))([^/&]{10,12})"""
            .toRegex()
        val groups = regex.find(url)?.groupValues
        return groups?.getOrNull(1)
    }

    fun plural(n: Int, zero: String, one: String, more: (Int) -> String): String {
        if (n <= 0) return zero
        if (n == 1) return one
        return more(n)
    }

    fun plural(n: Int, zero: String, one: String, more: String): String {
        return plural(n, zero, one) { more.replace("$0", "$it") }
    }

    fun number(d: Double, digits: Int = 2): String {
        val nf = NumberFormat.getInstance()
        nf.minimumFractionDigits = digits
        nf.maximumFractionDigits = digits
        nf.isGroupingUsed = true
        return nf.format(d)
    }

    inline fun whenNotEmpty(s: String?, cb: (String) -> Unit) {
        if (s?.trim()?.isNotBlank() != true) return
        cb(s)
    }

    fun generateQueryStr(vararg list: String): String
    {
        val r = mutableListOf<String>()

        for (s in list) {
            val sl = s.trim().lowercase()
            if (sl.isEmpty()) continue

            r.add(sl)

            var cur = sl
            val patterns = listOf(
                """(ã|â|á|à)""" to "a",
                """(ê|é|è)""" to "e",
                """(î|í|ì)""" to "i",
                """(õ|ô|ó|ò)""" to "o",
                """(û|ú|ù)""" to "u",
                """(ñ)""" to "n",
                """('|)""" to "",
                """(-|_)""" to " ",
                """ç""" to "c"
            )

            for (p in patterns) {
                cur = p.first.toRegex().replace(cur, p.second)
            }

            if (cur != sl && cur.isNotEmpty()) r.add(cur)
        }

        return r.joinToString(" ")
    }


}