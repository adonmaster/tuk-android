package com.adon.tuk.helpers

object CarColors {

    val palette = arrayOf("#FFBB86FC", "#FF000000", "#FFFFFFFF", "#F44336", "#E91E63", "#9C27B0", "#ff40a2", "#2196F3", "#00BCD4", "#009688", "#4CAF50", "#FFEB3B", "#FF9800", "#ff5722", "#795548", "#616161", "#607D8B")

}