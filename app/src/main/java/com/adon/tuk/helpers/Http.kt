package com.adon.tuk.helpers

import android.net.Uri
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.ANRequest
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.DownloadListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.StringRequestListener
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

object Http {

    fun url(uri: String): String {
        if (uri.startsWith("http", true)) return uri
        return "http://ec2-18-118-84-246.us-east-2.compute.amazonaws.com:4001/api/$uri"
    }

    fun download(uri: String, dir: File, filename: String) = Promise<File> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.download(u, dir.path, filename)
            .build()
            .startDownload(object : DownloadListener {
                override fun onDownloadComplete() {
                    resolve(File(dir, filename))
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    fun getMock(uri: String, params: List<Pair<String, Any>> = listOf(), res: JSONObject) = Promise<JSONObject> { resolve, reject ->
        Task.main(2000) {
            resolve(res)
        }
    }

    fun get(uri: String, params: List<Pair<String, Any>> = listOf()) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.get(u)
            .apply { params.forEach { addQueryParameter(mapOf(it)) } }
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    fun getString(uri: String, params: List<Pair<String,Any>> = listOf()) = Promise<String?> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.get(u)
            .apply { params.forEach { addQueryParameter(mapOf(it)) } }
            .build()
            .getAsString(object : StringRequestListener {
                override fun onResponse(response: String?) {
                    resolve(response)
                }
                override fun onError(anError: ANError?) {
                    reject(Str.coalesce(anError?.errorBody, anError?.localizedMessage, "Erro $uri"))
                }
            })
    }

    fun post(uri: String, params: HttpFormData?) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.post(u)
            .addJSONObjectBody(params?.get())
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    fun delete(uri: String, params: HttpFormData?) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.delete(u)
            .addJSONObjectBody(params?.get())
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    fun upload(uri: String, params: HttpMulti) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        val r = AndroidNetworking.upload(u)
            .apply { params.attachTo(this) }
            .build()

        r.getAsJSONObject(object : JSONObjectRequestListener {
            override fun onResponse(response: JSONObject?) {
                resolve(response!!)
            }
            override fun onError(anError: ANError?) {
                resolveError(anError, reject)
            }
        })
    }


    private fun resolveError(error: ANError?, reject: (String) -> Unit)
    {
        var message: String? = null

        when {
            error?.errorDetail == "connectionError" -> {
                message = "Erro de conexão. Sua internet está ok?"
            }
            error?.errorCode == 422 -> {
                var vfield = ""
                var vmsg = ""
                jex(error.errorBody) {
                    o("payload") {
                        o("errors") {
                            vfield = keys.first()
                            arrStr(vfield) { vmsg = it; false }
                        }
                    }
                }
                message = "$vfield: $vmsg"
            }
            else -> {
                // json error
                jex(error?.errorBody) {
                    message = str("message")
                        .takeIf { it?.trim()?.isEmpty() == false }
                        ?: str("exception")
                            .takeIf { it?.trim()?.isEmpty() == false }

                    o("errors") {
                        arrStr(keys[0]) {
                            message = it
                            false
                        }
                    }
                }
            }
        }

        // couldn't catch it
        reject(Str.coalesce(message, error?.toString(), "Error 61"))
    }

    fun buildParams() = HttpFormData()

    fun urlAttachment(remoteId: Long, path: String, isThumb: Boolean): String {
        if (remoteId > 0) {
            return url("attachment/${remoteId}") + ter(isThumb, "?thumb=", "")
        }
        return Uri.fromFile(File(path)).toString()
    }

}

class HttpFormData(private val jsonObject: JSONObject = JSONObject()) {
    fun put(key: String, value: String): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun putList(key: String, values: List<String>): HttpFormData {
        jsonObject.put(key, JSONArray(values))
        return this
    }
    fun put(key: String, value: Int): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun put(key: String, value: Long): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun put(key: String, value: Boolean): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun put(key: String, jsonArray: JSONArray?): HttpFormData {
        if (jsonArray!=null) jsonObject.put(key, jsonArray)
        return this
    }
    fun put(key: String, json: JSONObject?): HttpFormData {
        if (json!=null) jsonObject.put(key, json)
        return this
    }
    fun put(key: String, v: Double): HttpFormData {
        jsonObject.put(key, v)
        return this
    }
    fun get(): JSONObject {
        return jsonObject
    }
}


class HttpMulti {

    enum class ContentType(val raw: String) {
        JPEG("image/jpeg"),
        PNG("image/png"),
        PDF("application/pdf")
    }

    private val list: ArrayList<(ANRequest.MultiPartBuilder<ANRequest.MultiPartBuilder<*>>)->Unit>
            = arrayListOf()

    fun add(field: String, value: String): HttpMulti {
        list.add {
            it.addMultipartParameter(field, value)
        }
        return this
    }
    fun add(field: String, files: List<File>): HttpMulti {
        files.forEachIndexed { index, file ->
            list.add {
                it.addMultipartFile("$field[$index]", file)
            }
        }
        return this
    }
    fun add(field: String, file: File, content: HttpMulti.ContentType?=null): HttpMulti {
        list.add {
            it.addMultipartFile(field, file, content?.raw)
        }
        return this
    }

    fun attachTo(request: ANRequest.MultiPartBuilder<ANRequest.MultiPartBuilder<*>>) {
        list.forEach { item ->
            item(request)
        }
    }
}