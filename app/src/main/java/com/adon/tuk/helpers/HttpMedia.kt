package com.adon.tuk.helpers

import com.adon.tuk.Env
import com.adon.tuk.jmodels.JMedia
import java.io.File

object HttpMedia {

    fun upload(f: File, content: HttpMulti.ContentType) = Promise<JMedia> { resolve, reject ->

        Http.upload(Env.MEDIA_URL, HttpMulti().add("file", f, content))
            .then {
                resolve(JMedia.parse(it))
            }
            .catch(reject)
    }

}