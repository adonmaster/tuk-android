package com.adon.tuk.helpers
import android.graphics.*
import androidx.annotation.DrawableRes
import com.adon.tuk.App

object Bmp {

    fun getResizedBitmap(@DrawableRes id: Int, width: Int, height: Int): Bitmap? {
        val bmp = BitmapFactory.decodeResource(App.shared.resources, id)
        return getResizedBitmap(bmp, width, height)
    }

    fun getResizedBitmap(bitmap: Bitmap, width: Int, height: Int): Bitmap?
    {
        if (width == 0) return null

        val background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val originalWidth = bitmap.width.toFloat()
        val originalHeight = bitmap.height.toFloat()

        val canvas = Canvas(background)

        val scale: Float = (width / originalWidth).coerceAtMost(height / originalHeight)
        val xTranslation = (width - (originalWidth * scale)) / 2f
        val yTranslation = (height - (originalHeight * scale)) / 2f

        val transformation = Matrix()
        transformation.postTranslate(xTranslation, yTranslation)
        transformation.preScale(scale, scale)

        val paint = Paint()
        paint.isFilterBitmap = true

        canvas.drawBitmap(bitmap, transformation, paint)

        return background
    }

    fun trim(bmp: Bitmap): Bitmap
    {
        val pixels = IntArray(bmp.width * bmp.height)
        bmp.getPixels(pixels, 0, bmp.width, 0, 0, bmp.width, bmp.height)

        var minx = bmp.width
        var maxx = -1
        var miny = bmp.height
        var maxy = -1

        val indexer: (Int,Int)->Int = { x, y -> y * bmp.width + x }

        for (y in 0 until bmp.height) {
            for (x in 0 until bmp.width) {
                val cl = Color.alpha(pixels[indexer(x, y)])
                if (cl < 50) continue

                if (minx > x) minx = x
                if (maxx < x) maxx = x
                if (miny > y) miny = y
                if (maxy < y) maxy = y
            }
        }

        return if (maxx < minx || maxy < miny)
            bmp
        else Bitmap.createBitmap(
            bmp, minx, miny,
            maxx - minx + 1, maxy - miny + 1
        )
    }

    fun createEmpty(): Bitmap {
        return Bitmap.createBitmap(2, 2, Bitmap.Config.ARGB_8888)
    }

}