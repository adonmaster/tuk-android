package com.adon.tuk.helpers

object Num {

    fun max(vararg list: Double): Double {
        if (list.isEmpty()) return 0.0
        var r = list.first()
        for (d in list) {
            if (d > r) r = d
        }
        return r
    }
}