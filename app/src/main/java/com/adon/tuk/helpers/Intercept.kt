package com.adon.tuk.helpers

object Intercept {

    private val cache = ArrayList<InterceptListener>()

    val listeners
        get() = cache

    fun register(listener: InterceptListener) {
        if (! listeners.contains(listener)) listeners.add(listener)
    }

    fun unregister(listener: InterceptListener) {
        listeners.remove(listener)
    }

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T> retrieveFirst(key: String): T? {
        for (l in listeners) {
            val vv = l.intercept(key) ?: break
            if (vv is T) return vv
        }
        return null
    }

}

interface InterceptListener {

    fun intercept(key: String): Any?

}