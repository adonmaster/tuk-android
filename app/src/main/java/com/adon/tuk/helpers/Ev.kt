package com.adon.tuk.helpers

object Ev {

    private val cache = hashMapOf<String,ArrayList<Pair<Any,(Any?)->Unit>>>()

    fun register(key: String, owner: Any, cb: (Any?)->Unit) {
        val list = cache[key] ?: arrayListOf()
        list.add(owner to cb)
        cache[key] = list
    }

    fun unregister(owner: Any) {
        for (k in cache.keys) {
            val iterator = cache[k]!!.iterator()
            while (iterator.hasNext()) {
                val pair = iterator.next()
                if (pair.first == owner) iterator.remove()
            }
        }
    }

    fun fireSynced(key: String, payload: Any?=null) {
        val list = cache[key] ?: listOf()
        for (p in list) {
            p.second(payload)
        }
    }

    fun fire(key: String, payload: Any?=null) {
        Task.main {
            fireSynced(key, payload)
        }
    }

}