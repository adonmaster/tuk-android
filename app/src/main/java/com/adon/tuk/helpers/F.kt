package com.adon.tuk.helpers

import android.content.Context
import android.graphics.Bitmap
import com.adon.tuk.App
import java.io.File
import java.io.FileOutputStream


object F {

    private val context: Context get() = App.shared

    fun saveInternal(dir: String, filename: String, data: Bitmap): Promise<File> {
        return promiseIt(saveInternalSynced(dir, filename, data))
    }

    fun saveInternalSynced(dir: String, filename: String, data: Bitmap): File {
        val directory = File(context.filesDir, dir)
        directory.mkdirs()
        val file = File(directory, filename)

        val out = FileOutputStream(file)
        data.compress(Bitmap.CompressFormat.PNG, 90, out)

        out.flush()
        out.close()

        return file
    }

    fun copyInternal(src: File, dir: String, filename: String): File {
        val directory = File(context.filesDir, dir)
        directory.mkdirs()
        val r = File(directory, filename)
        src.copyTo(r, true)
        return r
    }

    fun pathToInternal(path: String?): File? {
        if (path===null) return null
        val f = File(context.filesDir, path)
        if (f.exists()) return f
        return null
    }

    fun saveExternalSynced(dir: String, filename: String, bitmap: Bitmap): File {
        val d = File(context.getExternalFilesDir(null), dir)
        d.mkdirs()
        val file = File(d, filename)

        val out = FileOutputStream(file)

        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out)

        out.flush()
        out.close()

        return file
    }

    fun copyExternal(srcFullPath: String?, destRelative: String?): File? {
        val fsrc = srcFullPath?.let { File(it) } ?: return null
        val fdest = destRelative?.let { File(context.getExternalFilesDir(null), it) }
            ?: return null

        fsrc.copyTo(fdest, true)

        return fdest;
    }

}
