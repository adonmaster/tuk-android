package com.adon.tuk.helpers

import android.view.View
import androidx.core.view.isVisible
import com.adon.tuk.repo.Repo

object AnimUtil {

    fun animateInfo(keyPref: String, wrapper: View?, info: View?)
    {
        if (Repo.userPref.get(keyPref, 0) < 2) {
            Repo.userPref.inc(keyPref)

            wrapper?.isVisible = true
            info?.isVisible = true

            info?.translationY = 40f
            info?.alpha = 0f
            info?.animate()?.setStartDelay(3000)
                ?.alpha(1f)?.translationY(0f)
                ?.withEndAction {
                    info.animate().setStartDelay(6000).setDuration(2000)
                        .withEndAction {
                            wrapper?.isVisible = false
                        }
                        .alpha(0f)
                        .start()
                }
                ?.start()
        }
    }

}