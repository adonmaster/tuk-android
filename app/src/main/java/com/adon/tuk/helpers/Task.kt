package com.adon.tuk.helpers

import android.os.Handler
import android.os.Looper
import org.threeten.bp.LocalDateTime

object Task {

    private val debounces by lazy<MutableList<String>> { mutableListOf() }
    private val timestamps = hashMapOf<String,LocalDateTime>()

    fun main(delay: Long = 0, closure: ()->Unit): Boolean {
        return Handler(Looper.getMainLooper()).postDelayed(closure, delay)
    }

    fun debounce(key: String, interval: Long, cb: ()->Unit) {
        debounces.add(key)
        main(interval) {
            debounces.remove(key)
            if (debounces.indexOf(key) == -1) cb()
        }
    }

    fun debounceLeading(key: String, interval: Long, cb: ()->Unit)
    {
        val dt = timestamps[key]
        val now = LocalDateTime.now()
        if (dt==null || now.isAfter(dt.plusNanos(1000 * interval))) {
            timestamps[key] = now
            main { cb() }
        }
    }

}