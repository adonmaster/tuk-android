package com.adon.tuk.helpers

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import com.nguyenhoanglam.imagepicker.model.GridCount
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.model.ImagePickerConfig
import com.nguyenhoanglam.imagepicker.model.RootDirectory
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePickerLauncher
import com.nguyenhoanglam.imagepicker.ui.imagepicker.registerImagePicker
import java.io.File


object ImgProvider {

    inline fun launch(f: Fragment, crossinline onSelected: (Image)->Unit): ImagePickerLauncher {
        return f.registerImagePicker { images ->
            if (images.isNotEmpty()) {
                onSelected(images[0])
            }
        }
    }

    fun fileFrom(context: Context, uri: Uri?): File?
    {
        if (uri == null) return null

        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = context.contentResolver.query(uri, projection, null, null, null) ?: return null
        val column_index: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val path = cursor.getString(column_index)
        cursor.close()

        val f = File(path)
        if (! f.exists()) return null

        return f
    }

    fun config(isMultiple: Boolean): ImagePickerConfig {
        return ImagePickerConfig(
            statusBarColor = "#000000",
            isLightStatusBar = false,
            isFolderMode = true,
            isMultipleMode = isMultiple,
            maxSize = 10,
            rootDirectory = RootDirectory.DOWNLOADS,
            subDirectory = "Photos",
            folderGridCount = GridCount(2, 4),
            imageGridCount = GridCount(3, 5),
            isShowNumberIndicator = true
        )
    }

}
