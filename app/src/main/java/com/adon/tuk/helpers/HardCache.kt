package com.adon.tuk.helpers

import com.adon.tuk.repo.Repo
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

object HardCache {

    private var observers = arrayListOf<Pair<String,ArrayList<Pair<Any,(Any)->Unit>>>>()

    fun listen(key: String, owner: Any, cb: (Any)->Unit) {
        val index = observers.indexOfFirst { it.first == key }
        if (index >= 0) {
            val list = observers[index].second
            list.add(owner to cb)
        } else {
            observers.add(key to arrayListOf(owner to cb))
        }
    }

    fun unlisten(owner: Any) {
        for (p in observers) {
            val iterator = p.second.iterator()
            while (iterator.hasNext()) {
                if (iterator.next() == owner) iterator.remove()
            }
        }
    }

    private fun fire(key: String, payload: Any) {
        val list = observers.find { it.first == key }
        list?.second?.forEach { it.second.invoke(payload) }
    }

    private fun putObject(key: String, cb: (JSONObject)->Unit, expirationInMin: Int) {
        val obj = JSONObject()
        cb(obj)
        val now = LocalDateTime.now()
        val dt = now.plusMinutes(expirationInMin.toLong())
        val expirationString = Dt.formatSql(dt)
        obj.put("expire_at", expirationString)

        Repo.userPref.put("HardCache-$key", obj.toString())
    }

    fun put(key: String, value: String, expirationInMin: Int) {
        putObject(key, { it.put("value", value) }, expirationInMin)
        fire(key, value)
    }

    private fun getObject(key: String): JSONObject? {
        val s = Repo.userPref.get("HardCache-$key") ?: return null
        val obj = JSONObject(s)
        val expiration = Dt.fromSql(obj.getString("expire_at"))!!
        if (expiration.isAfter(LocalDateTime.now())) return obj
        return null
    }

    fun get(key: String, def: String): String {
        val obj = getObject(key) ?: return def
        return obj.getString("value")
    }

}