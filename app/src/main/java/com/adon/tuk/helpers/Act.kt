package com.adon.tuk.helpers

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.adon.tuk.R
import com.adon.tuk.helpers.Num.max
import java.io.File

object Act {

    fun inflate(@LayoutRes layoutId: Int, parent: ViewGroup, attachToRoot: Boolean=false): View? {
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, attachToRoot)
    }

    fun openInstagram(context: Context, username: String)
    {
        val uri = "http://instagram.com/_u/$username"
        val instaIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        instaIntent.`package` = "com.instagram.android"

        if (isIntentAvailable(context, instaIntent)) {
            context.startActivity(instaIntent)
        } else {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
        }
    }

    fun openYoutube(activity: Activity?, youtubeID: String)
    {
        if (activity==null) return

        val intentApp = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$youtubeID"))
        val intentBrowser = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$youtubeID"))
        try {
            activity.startActivity(intentApp)
        } catch (ex: ActivityNotFoundException) {
            activity.startActivity(intentBrowser)
        }
    }

    private fun isIntentAvailable(context: Context, intent: Intent): Boolean {
        val list = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    fun shareImage(activity: Activity, imageFile: File) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile))
        activity.startActivity(Intent.createChooser(intent, "Compartilhar usando..."))
    }

    fun hideKeyboard(view: View?) {
        val context = view?.context ?: return
        val service = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        service?.hideSoftInputFromWindow(view.windowToken, 0)
    }
    fun hideKeyboard(fragment: Fragment?) {
        hideKeyboard(fragment?.view)
    }

    fun hideKeyboard(activity: Activity?) {
        val act = activity ?: return
        val service = act.getSystemService(Context.INPUT_METHOD_SERVICE)
                as? InputMethodManager
            ?: return
        val wd = act.currentFocus?.windowToken ?: return

        service.hideSoftInputFromWindow(wd, 0)
    }

    fun navigateWithAnimation(nav: NavController?, @IdRes id: Int, bundle: Bundle?) {
        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()
        nav?.navigate(id, bundle, navOptions)
    }

    fun fragReplace(fm: FragmentManager, @IdRes container: Int, fragment: Class<out Fragment>, bundle: Bundle?=null, reverseAnimation:Boolean=false) {
        fm.commit {
//            setCustomAnimations(R.anim.slide_in, R.anim.fade_out, R.anim.fade_in, R.anim.slide_out)

            if (reverseAnimation) {
                setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_left)
            } else {
                setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right)
            }
            replace(container, fragment, bundle)
        }
    }

    fun dpToPixel(context: Context, dp: Int): Int {
        val v = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), context.resources.displayMetrics)
        return v.toInt()
    }

    fun setupCalculator(
        vmValue: MutableLiveData<Double>, minValue: Double,
        btnMinus5: Button, btnMinus1: Button, btnPlus1: Button, btnPlus5: Button
    ) {
        val inc: (MutableLiveData<Double>, Double)->Unit = { v, factor ->
            v.value = max(v.value!! + factor, minValue )
        }
        btnMinus1.setOnClickListener { inc(vmValue, -1.0) }
        btnMinus5.setOnClickListener { inc(vmValue, -5.0) }
        btnPlus1.setOnClickListener { inc(vmValue, 1.0) }
        btnPlus5.setOnClickListener { inc(vmValue, 5.0) }
    }
}