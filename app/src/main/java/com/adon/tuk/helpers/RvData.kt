package com.adon.tuk.helpers

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.structs.Idable

class RvData<M: Idable, V: RecyclerView.ViewHolder>(
    private val adapter: RecyclerView.Adapter<V>,
    initialData: List<M> = listOf()
): DiffUtil.Callback()
{

    private var mOld = listOf<M>()
    private var mNew = listOf<M>()

    private var cbIsSame: ((M, M) -> Boolean)? = null

    fun setCbIsSame(cb: (M, M) -> Boolean): RvData<M, V> {
        this.cbIsSame = cb
        return this
    }

    init {
        mOld = initialData
    }

    fun update(newData: List<M>) {
        mNew = newData
        val results = DiffUtil.calculateDiff(this)
        mOld = mNew
        results.dispatchUpdatesTo(adapter)
    }

    fun updateSingle(vararg data: M) {
        val r = mOld.toMutableList()
        for (d in data) {
            val index = r.indexOfFirst { it.idable() == d.idable() }
            if (index >= 0) {
                r[index] = d
            } else {
                r.add(0, d)
            }
        }
        update(r)
    }

    fun items() = mOld

    //
    override fun getOldListSize() = mOld.size

    override fun getNewListSize() = mNew.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
        = mOld[oldItemPosition].idable() == mNew[newItemPosition].idable()

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return cbIsSame?.invoke(mOld[oldItemPosition], mNew[newItemPosition])
            ?: areItemsTheSame(oldItemPosition, newItemPosition)
    }

}
