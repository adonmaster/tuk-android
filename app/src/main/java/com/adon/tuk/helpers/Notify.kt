package com.adon.tuk.helpers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.adon.tuk.MainActivity
import com.adon.tuk.R
import com.google.firebase.messaging.RemoteMessage


class Notify(private val msg: RemoteMessage) {

    fun show(context: Context): Boolean {
        val title = msg.data["title"]
        val body = msg.data["body"]
        if (title?.isNotBlank() == true && body?.isNotBlank() == true) {
            show(context, title, body)
            return true
        }
        return false
    }

    private fun show(context: Context, title: String, body: String)
    {
        val intent = Intent(context, MainActivity::class.java)

        val pending = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notifyBuilder = NotificationCompat.Builder(context, "100")
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .setSound(notificationSound)
            .setContentIntent(pending)

        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        var notificationChannel: NotificationChannel? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel("100", "Notifications", NotificationManager.IMPORTANCE_DEFAULT)
            service.createNotificationChannel(notificationChannel)
        }

        service.notify(1, notifyBuilder.build())
    }

}