package com.adon.tuk.helpers

class Wait<T> {

    private var cb: ((T)->Boolean)? = null
    private var model: T? = null

    fun set(v: T?) {
        model = v
        var res: Boolean? = null
        v?.let { res = cb?.invoke(it) }
        res?.let { if ( ! it) { cb = null } }
    }

    /**
     *
     * if (then) returns false, i will remove the callback function once its triggered
     * if (then) returns true, i will keep the callback function, and reuse it once model is changed again
     */
    fun then(cb: (T)->Boolean) {
        this.cb = cb
        model?.let(cb)
    }

    fun release() {
        cb = null
        model = null
    }

}