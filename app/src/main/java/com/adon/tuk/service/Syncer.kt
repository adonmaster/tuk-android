package com.adon.tuk.service

import android.util.Log
import com.adon.tuk.App
import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.HttpFormData
import com.adon.tuk.helpers.Task
import com.adon.tuk.repo.Repo
import org.threeten.bp.LocalDateTime
import kotlin.properties.Delegates

object Syncer {

    private val cache by lazy { Repo.userPref.getList("Syncer.cache")?.toMutableList() ?: arrayListOf() }
    val errors = arrayListOf<String>()

    //
    private fun errorIt(key: String, errorMessage: String) {
        errors.add(Dt.formatBrDate(LocalDateTime.now()) + ": $key | " + errorMessage)
    }

    private fun register(key: String) {
        if ( ! cache.contains(key)) {
            cache.add(key)
            Repo.userPref.add("Syncer.cache", key)
        }

        Task.debounce("Syncer@work", 2000) {
            work()
        }
    }

    private fun unregister(key: String) {
        cache.remove(key)
        Repo.userPref.setList("Syncer.cache", cache)
    }

    //

    fun registerUserNameAvatar() { register("user.nameAvatar") }
    private fun unregisterUserNameAvatar() { unregister("user.nameAvatar") }

    fun registerFcmToken() { register("fcm.token") }
    private fun unregisterFcmToken() { unregister("fcm.token") }

    fun registerUserStatus() { register("user.status") }
    private fun unregisterUserStatus() { unregister("user.status") }


    private var working by Delegates.observable(0) { p, o, n ->

    }
    private fun work() {
        if (working > 0) return

        if (cache.contains("user.nameAvatar")) workUserNameAvatar()
        if (cache.contains("fcm.token")) workFcmToken()
        if (cache.contains("user.status")) workUserStatus()
    }

    private fun workUserNameAvatar()
    {
        val user = App.user.value ?: return

        val form = HttpFormData().put("name", user.name)
        user.avatar_uid?.let { form.put("avatar_uid", it) }
        user.avatar?.let { form.put("avatar_full", it) }
        user.avatar_thumb?.let { form.put("avatar_thumb", it) }

        working++
        Http.post("user/update-name-avatar", form)
            .then {
                unregisterUserNameAvatar()
            }
            .catch {
                errorIt("workUserNameAvatar", it)
            }
            .always { working-- }
    }

    private fun workFcmToken()
    {
        App.user.value ?: return
        val token = FcmService.getStoredToken() ?: return

        working++
        Http.post("user/mobile", HttpFormData().put("service", "fcm").put("token", token))
            .then {
                FcmService.setTokenSent(true)
                unregisterFcmToken()
            }
            .catch {
                errorIt("workFcmToken", it)
            }
            .always { working-- }
    }

    private fun workUserStatus()
    {
        App.user.value ?: return

        working++
        Http.getString("status")
            .then {
                Repo.userPref.putStatus(it)
                unregisterUserStatus()
            }
            .catch {
                errorIt("workUserStatus", it)
            }
            .always { working-- }
    }

}