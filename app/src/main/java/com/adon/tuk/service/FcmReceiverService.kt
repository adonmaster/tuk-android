package com.adon.tuk.service

import com.adon.tuk.AdActivity
import com.adon.tuk.App
import com.adon.tuk.MainActivity
import com.adon.tuk.db.DB
import com.adon.tuk.helpers.Notify
import com.adon.tuk.helpers.Task
import com.adon.tuk.models.NotificationModel
import com.adon.tuk.repo.Repo
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FcmReceiverService: FirebaseMessagingService() {

    override fun onNewToken(p0: String)
    {
        super.onNewToken(p0)
        FcmService.setStoredToken(p0)
        FcmService.setTokenSent(false)

        Syncer.registerFcmToken()
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        Notify(p0).show(this)

        Task.main {

            MainActivity.shared?.outFcmNotify(p0.data)
            AdActivity.shared?.outFcmNotify(p0.data)

            val payload = p0.data["payload"]?.takeIf { it.isNotBlank() }
            if (payload != null) {
                val msg = when(payload) {
                    "driver.accepted" -> "Sua requisição para motorista foi aprovada!"
                    "ping" -> "Pong!"
                    else -> null
                }
                if (msg != null) {
                    val model = Repo.notification.create(App.userId, p0.data["body"] ?: msg, payload)
                    MainActivity.shared?.outNotify(model)
                }
            }

        }

    }

}