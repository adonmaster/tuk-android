package com.adon.tuk.service

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Service
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.Looper
import android.util.Log
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.HttpFormData
import com.google.android.gms.location.*

class LocationService: Service() {

    val UPDATE_INTERVAL = 60_000L
    val UPDATE_MIN_INTERVAL = UPDATE_INTERVAL
    val UPDATE_MAX_INTERVAL = UPDATE_INTERVAL * 4
    val UPDATE_MIN_DISPLACEMENT = 60f

    //
    companion object {
        fun startAndBind(activity: Activity, serviceConnection: ServiceConnection) {
            val intent = Intent(activity, LocationService::class.java)
            activity.startService(intent)
            activity.bindService(intent, serviceConnection, BIND_NOT_FOREGROUND)
        }
        fun unbind(activity: Activity, serviceConnection: ServiceConnection) {
            activity.unbindService(serviceConnection)
        }
    }

    //
    private lateinit var mRequest: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private lateinit var mClient: FusedLocationProviderClient

    //
    open inner class LocalBinder: Binder() {
        fun getService() = this@LocationService
    }
    private val binder = LocalBinder()


    //
    override fun onCreate() {
        super.onCreate()
        Log.i("adon", "oncreate")

        mClient = LocationServices.getFusedLocationProviderClient(this)

        mRequest = LocationRequest.create()
            .setFastestInterval(UPDATE_MIN_INTERVAL)
            .setInterval(UPDATE_INTERVAL)
            .setSmallestDisplacement(UPDATE_MIN_DISPLACEMENT)
            .setMaxWaitTime(UPDATE_MAX_INTERVAL)

        mLocationCallback = object: LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                onLocation(result)
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    override fun onBind(p0: Intent?)= binder
    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    private fun onLocation(result: LocationResult) {
        val params = HttpFormData()
            .put("lat", result.lastLocation.latitude)
            .put("lng", result.lastLocation.longitude)
        Http.post("driver/position", params)
            .then { json ->
                Log.i("adon", json.toString())
            }
            .catch {
                Log.i("adon", it)
            }
//        Log.i("adon", "latitude: ${result.lastLocation.latitude}")
    }

    override fun onDestroy() {
        super.onDestroy()
        stopUpdates()
    }

    private var startedUpdates = false
    @SuppressLint("MissingPermission")
    fun startUpdates() {
        if (startedUpdates) return

        mClient.requestLocationUpdates(mRequest, mLocationCallback, Looper.myLooper()!!)
        startedUpdates = true
    }

    fun stopUpdates() {
        if (startedUpdates) mClient.removeLocationUpdates(mLocationCallback)
        startedUpdates = false
    }

}