package com.adon.tuk.service

import android.content.Context
import com.adon.tuk.repo.Repo
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.messaging.FirebaseMessaging


object FcmService {

    private const val keyTokenSent = "FCM_TOKEN_SENT"

    fun getStoredToken(): String? {
        return Repo.userPref.get("FCM_TOKEN")
    }

    fun init(context: Context) {
        FirebaseApp.initializeApp(context)
    }

    fun setStoredToken(s: String?) {
        if (s == null) {
            Repo.userPref.remove("FCM_TOKEN")
        } else {
            Repo.userPref.put("FCM_TOKEN", s)
        }
    }

    fun isTokenSent(): Boolean {
        return Repo.userPref.get("FCM_TOKEN_SENT", false)
    }

    fun setTokenSent(option: Boolean=true) {
        Repo.userPref.put("FCM_TOKEN_SENT", option)
    }

    fun mainActivityStart()
    {
        if ( ! isTokenSent()) Syncer.registerFcmToken()

        val token = getStoredToken()
        if (token == null) {
            FirebaseMessaging.getInstance().token.addOnSuccessListener {
                setStoredToken(it)
                Syncer.registerFcmToken()
            }
        }
    }

}