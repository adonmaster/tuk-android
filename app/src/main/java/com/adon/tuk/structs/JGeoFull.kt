package com.adon.tuk.structs

import com.adon.tuk.helpers.ResultEx
import com.adon.tuk.helpers.ter
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import org.json.JSONObject
import java.util.ArrayList

class JGeoFull(
    val route: List<List<LatLng>>,
    val duration: Int, val durationStr: String,
    val distance: Int, val distanceStr: String,
    val origin: String, val destination: String
)
{
    companion object {

        fun parse(route: JSONObject, distance: JSONObject): ResultEx<JGeoFull>
        {
            var err = getError(distance)
            if (err != null) return ResultEx.fail(err)

            val row = distance.getJSONArray("rows").getJSONObject(0)
            val element = row.getJSONArray("elements").getJSONObject(0)

            val destinationAddr = distance.getJSONArray("destination_addresses").getString(0)
            val originAddr = distance.getJSONArray("origin_addresses").getString(0)

            val distanceStr = element.getJSONObject("distance").getString("text")
            val distanceInt = element.getJSONObject("distance").getInt("value")

            val durationStr = element.getJSONObject("duration").getString("text")
            val duration = element.getJSONObject("duration").getInt("value")

            //
            err = getError(route)
            if (err != null) return ResultEx.fail(err)

            val path: MutableList<List<LatLng>> = ArrayList()
            val routes = route.getJSONArray("routes")
            val legs = routes.getJSONObject(0).getJSONArray("legs")
            val steps = legs.getJSONObject(0).getJSONArray("steps")
            for (i in 0 until steps.length()) {
                val points = steps
                    .getJSONObject(i)
                    .getJSONObject("polyline")
                    .getString("points")
                path.add(PolyUtil.decode(points))
            }

            //

            val model = JGeoFull(path, duration, durationStr, distanceInt, distanceStr, originAddr, destinationAddr)
            return ResultEx.success(model)
        }

        private fun getError(json: JSONObject): String? {
            if (json.getString("status") != "OK") {
                return ter(
                    json.has("message"),
                    json.getString("message"),
                    "geofull: erro ao carregar distance"
                )
            }
            return null
        }
    }



}