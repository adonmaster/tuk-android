package com.adon.tuk.structs

import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

class GeoRoute(
    val origin: LatLng, val destination: LatLng, val paths: List<List<LatLng>>
)