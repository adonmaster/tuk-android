package com.adon.tuk.structs

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import org.json.JSONArray
import org.json.JSONObject

object GeoPath {

    fun from(array: JSONArray?): List<List<LatLng>>?
    {
        if (array == null) return null

        val r = arrayListOf<List<LatLng>>()
        for (i in 0 until array.length()) {
            val iarray = array.getJSONArray(i)
            val list = arrayListOf<LatLng>()
            for (j in 0 until iarray.length()) {
                val obj = iarray.getJSONObject(j)
                val lat = obj.getDouble("lat")
                val lng = obj.getDouble("lng")
                list.add(LatLng(lat, lng))
            }
            r.add(list)
        }
        return r
    }

    fun from(s: String): List<List<LatLng>>? = from(JSONArray(s))

    fun toJsonArray(path: List<List<LatLng>>): JSONArray {
        val root = JSONArray()

        for (i in path) {
            val row = JSONArray()
            for (j in i) {
                val obj = JSONObject()
                obj.put("lat", j.latitude)
                obj.put("lng", j.longitude)
                row.put(obj)
            }
            root.put(row)
        }

        return root
    }

    fun toString(path: List<List<LatLng>>) = toJsonArray(path).toString()


    fun generateBounds(path: List<List<LatLng>>): LatLngBounds.Builder {
        val r = LatLngBounds.Builder()
        for (row in path) {
            for (j in row) {
                r.include(j)
            }
        }
        return r
    }


}