package com.adon.tuk.structs

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng

class AdRequestStruct(

    val origin: LatLng,
    val originLine: String,
    val destination: LatLng,
    val destinationLine: String,

    val routePath: List<List<LatLng>>,

    val routeDurationStr: String,
    val routeDuration: Int,
    val routeDistanceStr: String,
    val routeDistance: Int
): Parcelable
{
    constructor(parcel: Parcel) : this(

        parcel.readParcelable(LatLng::class.java.classLoader)!!,
        parcel.readString()!!,
        parcel.readParcelable(LatLng::class.java.classLoader)!!,
        parcel.readString()!!,

        GeoPath.from(parcel.readString()!!)!!,

        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeParcelable(origin, flags)
        parcel.writeString(originLine)
        parcel.writeParcelable(destination, flags)
        parcel.writeString(destinationLine)

        parcel.writeString(GeoPath.toString(routePath))

        parcel.writeString(routeDurationStr)
        parcel.writeInt(routeDuration)
        parcel.writeString(routeDistanceStr)
        parcel.writeInt(routeDistance)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AdRequestStruct> {
        override fun createFromParcel(parcel: Parcel): AdRequestStruct {
            return AdRequestStruct(parcel)
        }

        override fun newArray(size: Int): Array<AdRequestStruct?> {
            return arrayOfNulls(size)
        }
    }

}