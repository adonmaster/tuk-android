package com.adon.tuk.structs

class GeoDistance(
    val duration: Int, val durationStr: String,
    val distance: Int, val distanceStr: String,
    val origin: String, val destination: String
)