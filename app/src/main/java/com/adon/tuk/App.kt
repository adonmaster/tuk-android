package com.adon.tuk

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.adon.tuk.db.DB
import com.adon.tuk.models.User
import com.adon.tuk.repo.Repo
import com.adon.tuk.seeders.CitySeeder
import com.adon.tuk.service.FcmService
import com.adon.tuk.service.Syncer
import com.androidnetworking.AndroidNetworking
import com.jakewharton.threetenabp.AndroidThreeTen
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class App: Application() {

    companion object {
        lateinit var shared: App
        val user get() = shared.user
        val userId get() = user.value?.id ?: 0
    }

    private var user = MutableLiveData<User?>(null)

    private fun test() {

//        val token = FcmService.getStoredToken() ?: ""
//        Log.i("adon", token)
    }

    override fun onCreate() {
        super.onCreate()

        shared = this

        test()

        configDateTime()
        configHttp()
        configDb(false)
        configSeed()

        FcmService.init(this)

        userLoad()
    }

    private fun configSeed() {
        CitySeeder(this@App).process()
    }

    private fun configDateTime() {
        AndroidThreeTen.init(this)
    }

    private fun configHttp() {
        val config = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val authRequest = chain.request()!!.newBuilder()
                    .addHeader("Accept", "application/json")
                    .also { r ->
                        user.value?.token?.let { tk ->
                            r.addHeader("Authorization","Bearer $tk")
                        }
                    }
                    .build()
                chain.proceed(authRequest)
            }
            .addInterceptor { chain ->
                val r = chain.proceed(chain.request())
                if (r.code() == 401) {
                    Repo.user.clearActive()
                    MainActivity.shared?.outUserInvalidated()
                }
                r
            }
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        AndroidNetworking.initialize(applicationContext, config)
        AndroidNetworking.enableLogging()
    }

    private fun configDb(deleteDatabase: Boolean)
    {
        deleteDatabase && deleteDatabase("main.room")

        DB.config(this)
        DB.seed()
    }

    fun userLoad() {
        user.value = Repo.user.findActive()
    }

    fun userFlush() {
        val u = user.value ?: return

        user.value = u
        Repo.user.update(u.id, u.name, u.avatar_uid, u.avatar, u.avatar_thumb)

        Syncer.registerUserNameAvatar()
    }

}