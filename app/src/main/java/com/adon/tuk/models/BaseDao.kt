package com.adon.tuk.models

import androidx.room.*

abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(model: T): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(model: T)

    @Delete
    abstract fun delete(model: T)

    // override it always
    abstract fun find(id: Long): T?

}