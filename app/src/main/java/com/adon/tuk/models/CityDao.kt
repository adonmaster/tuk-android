package com.adon.tuk.models

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

@Dao
abstract class CityDao: BaseDao<City>() {

    @Query("select * from cities where id = :id LIMIT 1")
    abstract override fun find(id: Long): City?

    @Query("select count(*) from cities")
    abstract fun hasAny(): List<Int>

    @Insert
    abstract fun insertAll(cities: List<City>)

    @Query("delete from cities")
    abstract fun deleteAll()

    @Transaction
    open fun seed(cities: List<City>) {
        deleteAll()
        insertAll(cities)
    }

    @Query("select * from cities where name_query like :s order by star desc, name limit 30")
    abstract fun query(s: String): List<City>

}