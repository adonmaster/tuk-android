package com.adon.tuk.models

import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.adon.tuk.helpers.LightCache

abstract class BaseModel {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @Ignore
    fun isNew() = id == 0L

}