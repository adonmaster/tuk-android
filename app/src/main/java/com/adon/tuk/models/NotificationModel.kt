package com.adon.tuk.models

import androidx.room.Entity
import androidx.room.Ignore
import com.adon.tuk.structs.Idable
import org.threeten.bp.LocalDateTime

@Entity(tableName = "notifications")
class NotificationModel: BaseModel(), Idable {

    var user_id: Long = 0
    lateinit var msg: String
    lateinit var key: String
    var seen = false
    var created_at = LocalDateTime.now()

    @Ignore
    val p = NotificationModelPresenter(this)

    override fun idable() = id

}