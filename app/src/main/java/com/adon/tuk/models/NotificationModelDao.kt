package com.adon.tuk.models

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
abstract class NotificationModelDao: BaseDao<NotificationModel>() {

    @Query("select * from notifications where id = :id LIMIT 1")
    abstract override fun find(id: Long): NotificationModel?

    @Query("select count(*) from notifications where user_id = :user_id and seen = 0")
    abstract fun countNotSeen(user_id: Long): Int

    @Query("select * from notifications where user_id = :userId order by id desc limit 150")
    abstract fun getAll(userId: Long): List<NotificationModel>

    @Query("update notifications set seen = 1 where id in (:ids)")
    abstract fun seen(ids: List<Long>)

    @Query("delete from notifications where user_id = :user_id")
    abstract fun deleteAll(user_id: Long)

}