package com.adon.tuk.models

import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class UserDao: BaseDao<User>() {

    @Query("select * from users where id = :id LIMIT 1")
    abstract override fun find(id: Long): User?

    @Query("select * from users where active = 1 LIMIT 1")
    abstract fun findActive(): User?

    @Query("select * from users where email like :email LIMIT 1")
    abstract fun findBy(email: String): User?

    @Query("update users set active = 0")
    abstract fun clearActive()

}