package com.adon.tuk.models

import androidx.room.Entity
import com.adon.tuk.structs.Idable

@Entity(tableName = "cities")
class City: BaseModel(), Idable {

    lateinit var name: String
    lateinit var name_query: String
    lateinit var state: String
    lateinit var uf: String
    var star = 0

    override fun idable() = id

}