package com.adon.tuk.models

import androidx.room.Entity

@Entity(tableName = "users")
class User: BaseModel() {

    var remote_id: Long = 0
    lateinit var name: String
    lateinit var email: String
    var avatar_uid: String? = null
    var avatar: String? = null
    var avatar_thumb: String? = null
    var token: String? = null
    var active: Boolean = false

}