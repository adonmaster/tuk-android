package com.adon.tuk.models

class NotificationModelPresenter(private val model: NotificationModel) {

    fun icon(): CharSequence {
        return when(model.key) {
            "driver.accepted" -> "\uF2C2"
            else -> "\uF0F3"
        }
    }

}
