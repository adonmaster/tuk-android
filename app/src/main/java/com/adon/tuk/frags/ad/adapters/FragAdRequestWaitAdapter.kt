package com.adon.tuk.frags.ad.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.Img
import com.adon.tuk.helpers.Task
import com.adon.tuk.rmodels.DriverDummyRModel
import java.text.DecimalFormat

class FragAdRequestWaitAdapter(private val listener: Listener):
    RecyclerView.Adapter<FragAdRequestWaitAdapter.VH>()
{
    //
    val data = arrayListOf<DriverDummyRModel>()

    //
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_ad_request_wait, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int)
    {
        val m = data[position]

        Img.loadAvatar(m.avatar, holder.uiAvatar)
        holder.uiTitle?.text = m.name
        holder.uiArrival?.text = "Tempo de chegada: ${m.arrival}mins"
        holder.uiMemberSince?.text = "Membro desde: ${Dt.format(m.memberSince)}"
        holder.uiLike?.text = "${m.like} likes"
        holder.uiDislike?.text = "${m.dislike} dislikes"
        holder.uiPrice?.text = "R$ " + DecimalFormat("#,#00.00").format(m.proposal_value)

        Task.main {
            holder.uiProgress?.max = 100
            val progress = m.like.toFloat() / (m.like + m.dislike) * 100
            holder.uiProgress?.progress = progress.toInt()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return data[position].id.toLong()
    }

    //
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val uiAvatar: ImageView? = itemView.findViewById(R.id.img_avatar)
        val uiTitle: TextView? = itemView.findViewById(R.id.lbl_title)
        val uiArrival: TextView? = itemView.findViewById(R.id.lbl_arrival)
        val uiMemberSince: TextView? = itemView.findViewById(R.id.lbl_member_since)
        val uiLike: TextView? = itemView.findViewById(R.id.lbl_like)
        val uiDislike: TextView? = itemView.findViewById(R.id.lbl_dislike)
        val uiProgress: ProgressBar? = itemView.findViewById(R.id.progress)
        val uiPrice: TextView? = itemView.findViewById(R.id.lbl_price)

        init {
            itemView.setOnClickListener {
                val position = absoluteAdapterPosition
                val model = data[position]
                listener.onSelect(model, position)
            }
        }
    }

    //
    interface Listener {
        fun onSelect(model: DriverDummyRModel, position: Int)
    }

}