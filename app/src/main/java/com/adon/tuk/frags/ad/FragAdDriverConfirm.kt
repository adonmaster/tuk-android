package com.adon.tuk.frags.ad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.FragAdDriverConfirmBinding

class FragAdDriverConfirm: Fragment() {

    //
    private var _ui: FragAdDriverConfirmBinding? = null
    private val ui get() = _ui!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        _ui = FragAdDriverConfirmBinding.inflate(inflater, container, false)

        return ui.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}