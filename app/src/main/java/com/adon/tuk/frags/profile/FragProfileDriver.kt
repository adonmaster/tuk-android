package com.adon.tuk.frags.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.adon.tuk.DriverRequestActivity
import com.adon.tuk.R
import com.adon.tuk.databinding.FragProfileDriverBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.ter
import com.adon.tuk.jmodels.JDriver
import com.adon.tuk.repo.Repo
import org.json.JSONObject

class FragProfileDriver: Fragment() {

    companion object {
        lateinit var shared: FragProfileDriver
    }

    //
    private var _ui: FragProfileDriverBinding? = null
    private val ui get() = _ui!!

    //
    private lateinit var mDriverRequestLauncher: ActivityResultLauncher<Intent>

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        shared = this
        _ui = FragProfileDriverBinding.inflate(inflater, container, false)

        mDriverRequestLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                Repo.userPref.put("driver.status", "pending")
                updateFrag()
            }
        }

        return ui.root
    }

    fun outLaunchDriverRequest() {
        mDriverRequestLauncher.launch(Intent(requireActivity(), DriverRequestActivity::class.java))
    }

    fun outNavigateToRequest()
    {
        Repo.userPref.put("driver.status", "request")

        val nav = ui.navProfileDriverHost.findNavController()
        Act.navigateWithAnimation(nav, R.id.nav_frag_profile_driver_request, null)
    }

    private fun updateFrag() {
        val nav = ui.navProfileDriverHost.findNavController()
        val status = Repo.userPref.get("driver.status")
        when (status ?: "request") {
            "request" -> nav.navigate(R.id.nav_frag_profile_driver_request)
            "pending" -> nav.navigate(R.id.nav_frag_profile_driver_pending)
            "ok" -> nav.navigate(R.id.nav_frag_profile_driver_ok)
        }
    }

    private fun requestDriverMyself()
    {
        Http.getString("driver/myself")
            .then { s ->
                if (s?.isNotEmpty() == true) {
                    JDriver.parse(JSONObject(s)) // validate
                    Repo.userPref.put("driver.status", "ok")
                    Repo.userPref.put("driver.model", s)
                }
                updateFrag()
            }
            .catch {
                DialogMini.error(this, it)
            }
    }

    override fun onStart() {
        super.onStart()
        updateFrag()

        requestDriverMyself()
    }

}