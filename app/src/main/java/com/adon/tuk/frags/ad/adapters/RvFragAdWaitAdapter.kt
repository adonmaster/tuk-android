package com.adon.tuk.frags.ad.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JProposal

class RvFragAdWaitAdapter(private val listener: Listener): RecyclerView.Adapter<RvFragAdWaitAdapter.VH>()
{

    val data = RvData<JProposal, VH>(this)
        .setCbIsSame { a, b -> a.price == b.price }

    //
    fun update(data: List<JProposal>) {
        this.data.update(data)
    }

    override fun getItemCount(): Int {
        val n = data.items().size
        listener.onEmpty(n == 0)
        return n
    }

    //
    override fun getItemId(position: Int): Long {
        return data.items()[position].id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {

        val v = Act.inflate(R.layout.rv_frag_ad_wait, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: RvFragAdWaitAdapter.VH, position: Int) {
        val m = data.items()[position]

        Img.loadAvatar(m.driver_avatar_thumb, holder.avatar)
        holder.title.text = "${m.driver_name_first} ${m.driver_name_last}"
        holder.arrival.text = "~ ${m.arrival} mins"
        holder.memberSince.text = "Membro desde: ${Dt.fromNow(m.created_at)} (${Dt.formatBrDate(m.created_at)})"
        holder.price.text = "R$ ${Str.number(m.price)}"
    }


    //

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val avatar: ImageView by lazy { itemView.findViewById(R.id.img_avatar) }
        val title: TextView by lazy { itemView.findViewById(R.id.lbl_title) }
        val arrival: TextView by lazy { itemView.findViewById(R.id.lbl_arrival) }
        val memberSince: TextView by lazy { itemView.findViewById(R.id.lbl_member_since) }
        val price: TextView by lazy { itemView.findViewById(R.id.lbl_price) }

        init {
            itemView.setOnClickListener {
                val position = absoluteAdapterPosition
                val model = data.items()[position]
                listener.onSelect(model, position)
            }
        }
    }

    interface Listener {
        fun onEmpty(option: Boolean)
        fun onSelect(model: JProposal, position: Int)
    }

}