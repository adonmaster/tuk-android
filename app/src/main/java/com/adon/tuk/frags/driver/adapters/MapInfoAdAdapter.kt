package com.adon.tuk.frags.driver.adapters

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.adon.tuk.R
import com.adon.tuk.helpers.Dt
import com.adon.tuk.helpers.Img
import com.adon.tuk.helpers.Str
import com.adon.tuk.jmodels.JAd
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import de.hdodenhof.circleimageview.CircleImageView

class MapInfoAdAdapter(private val inflater: LayoutInflater): GoogleMap.InfoWindowAdapter {

    private val root by lazy { inflater.inflate(R.layout.map_info_ad, null) }
    private val title: TextView by lazy { root.findViewById(R.id.lbl_title) }
    private val avatar: CircleImageView by lazy { root.findViewById(R.id.img_avatar) }
    private val member_since: TextView by lazy { root.findViewById(R.id.lbl_member_since) }
    private val route_info: TextView by lazy { root.findViewById(R.id.lbl_route_info) }
    private val price: TextView by lazy { root.findViewById(R.id.lbl_price) }

    override fun getInfoContents(marker: Marker): View? {
        return root
    }

    fun update(ad: JAd)
    {
        title.text = ad.user_name

        Img.loadAvatar(ad.user?.avatar_thumb, this.avatar)

        member_since.text = "${Dt.fromNow(ad.created_at)} (${Dt.formatBrDate(ad.created_at)})"

        route_info.text = "${ad.distance_s}, ${ad.duration_s}"

        price.text = "R$ " + Str.number(ad.price)
    }

    override fun getInfoWindow(p0: Marker): View? {
        return null
    }
}