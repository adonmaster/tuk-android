package com.adon.tuk.frags.ad

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.helpers.Num

class FragAdRequestVM: ViewModel() {

    val loading = MutableLiveData(false)

    val price = MutableLiveData(0.0)

    fun calculateSuggestedPrice(distanceInMeters: Int): Double {
        return Num.max(10.0, distanceInMeters / 1000 * 1.65)
    }

    fun priceInc(factor: Double) {
        val d = price.value ?: 0.0
        this.price.value = Num.max(10.0, d.plus(factor))
    }

}