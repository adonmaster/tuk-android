package com.adon.tuk.frags.driver

import android.Manifest
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.CompoundButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.App
import com.adon.tuk.MainActivity
import com.adon.tuk.R
import com.adon.tuk.databinding.FragDriverBinding
import com.adon.tuk.dialogs.DialogDriverAd
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.frags.driver.adapters.MapInfoAdAdapter
import com.adon.tuk.frags.driver.adapters.RvFragDriverAdapter
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JAd
import com.adon.tuk.jmodels.JDriver
import com.adon.tuk.repo.Repo
import com.adon.tuk.service.LocationService
import com.adon.tuk.structs.GeoPath
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.*

@DelicateCoroutinesApi
class FragDriver: Fragment(), DialogDriverAd.Listener {

    //
    companion object {
        var shared: FragDriver? = null
    }

    //
    private lateinit var vm: FragDriverVM
    private var _ui: FragDriverBinding? = null
    private val ui get() = _ui!!

    //
    private val mAuthorized = MutableLiveData(false)
    private lateinit var mLocationClient: FusedLocationProviderClient
    private var mBinder = Wait<LocationService.LocalBinder>()
    private val mServiceConn = object: ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            mBinder.set(p1 as? LocationService.LocalBinder)
        }
        override fun onServiceDisconnected(p0: ComponentName?) {
            mBinder.release()
        }
    }
    private lateinit var mAdapter: RvFragDriverAdapter
    private val cache = LightCache(25)

    //
    private lateinit var uiMapInfoAdapter: MapInfoAdAdapter

    //
    private var mModel: JDriver? = null

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vm = ViewModelProvider(this)[FragDriverVM::class.java]
        _ui = FragDriverBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        ui.mapView.onCreate(savedInstanceState)
        ui.mapView.alpha = .01f
        ui.mapView.getMapAsync { g ->

            g.uiSettings.isMyLocationButtonEnabled = false
            g.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(),R.raw.map_style_silver_2))
            ui.mapView.animate().alpha(1f).setStartDelay(500).start()

            uiMapInfoAdapter = MapInfoAdAdapter(layoutInflater)
            g.setInfoWindowAdapter(uiMapInfoAdapter)

        }

        //
        val onCheckedOnlineListener: (CompoundButton, Boolean) -> Unit = { v, b ->
            v.text = ter(b, "Online", "Offline")
            v.setTextColor(Cl.ter(b, R.color.md_green_500, R.color.md_grey_500))
        }
        ui.swOnline.setOnCheckedChangeListener(onCheckedOnlineListener)
        ui.swOnline.isChecked = false
        onCheckedOnlineListener(ui.swOnline, false)

        //
        ui.btnMyLocation.setOnClickListener {
            locateItSelf()
        }

        //
        ui.swOnline.setOnClickListener {
            if ( ! mAuthorized.value!!) return@setOnClickListener
            Task.debounce("FragDriver@swOnline@OnClick", 1500) {
                requestOnline(ui.swOnline.isChecked)
            }
        }

        ui.rv.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        ui.rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_slide_right)
        mAdapter = RvFragDriverAdapter(object : RvFragDriverAdapter.Listener {
            override fun onSelected(item: JAd) {
            }
            override fun onEmpty(option: Boolean) {
                ui.lblRvEmpty.isVisible = option
            }
        })
        ui.rv.adapter = mAdapter
        mAdapter.selected.observe(viewLifecycleOwner) {
            val model = mAdapter[it]
            updateDesktopAd(model)
        }

        //
        vm.loading.observe(viewLifecycleOwner) {
            ui.uiProgress.isVisible = it > 0
            ui.swOnline.isEnabled = it == 0
        }

    }

    private fun requestOnline(option: Boolean)
    {
        vm.incLoading(1)
        Http.post("driver/online", HttpFormData().put("option", option))
            .then {
                afterRequestOnline(option)
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.incLoading(-1)
            }
    }

    private fun serviceOnlineUpdate(option: Boolean) {
        mBinder.then {
            if (option) {
                it.getService().startUpdates()
            } else {
                it.getService().stopUpdates()
            }
            true
        }
    }

    private fun updateDesktop()
    {
        val driver = mModel ?: return

        ui.swOnline.isChecked = driver.online
        serviceOnlineUpdate(driver.online)
    }

    private lateinit var mLastModel: JAd
    override fun dialogDriverAdRequest() = mLastModel

    private fun updateDesktopAd(model: JAd?) = Task.debounce("FragDriver@updateDesktopAd", 500) {

        Log.i("jamanta", "updateDesktopAd: ${ter(model==null, { 0 }, { model!!.id })}")

        ui.mapView.getMapAsync { g ->

            g.clear()

            if (model == null) return@getMapAsync
            mLastModel = model

            uiMapInfoAdapter.update(model)

            g.addMarker(MarkerOptions().title("Partida")
                .position(model.origin()))
                ?.showInfoWindow()

            g.addMarker(MarkerOptions().title("Chegada")
                .position(model.destination()))

            g.setOnInfoWindowClickListener {
                DialogDriverAd.show(childFragmentManager)
            }

            val promise: ()->Promise<JAd> = { Promise { resolve, reject ->
                Http.getString("ad/${model.id}")
                    .then { s ->
                        resolve(JAd.parse(s)!!)
                    }.catch(reject)
            }}

            vm.incLoading(1)
            cache.getPromise("ad/${model.id}", promise)
                .then { fullModel ->
                    if (shared == null) return@then
                    val route = fullModel.route ?: return@then

                    val bounds = GeoPath.generateBounds(route)
                    g.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 55), object: GoogleMap.CancelableCallback {
                        override fun onCancel() {}
                        override fun onFinish() { Geo.routeLoadPath(g, route) }
                    })

                }
                .catch {
                    DialogMini.error(this, it)
                }
                .always {
                    vm.incLoading(-1)
                }

        }

    }

    private fun afterRequestOnline(option: Boolean) {
        mModel!!.online = option
        HardCache.put("driver.model", mModel!!.toJSON(), 60)

        serviceOnlineUpdate(option)
    }

    private fun requestMyself(force: Boolean)
    {
        if (App.user.value == null) return

        if ( ! force) {
            val modelStr = HardCache.get("driver.model", "")
            if (modelStr.isNotEmpty()) {
                mModel = JDriver.parse(modelStr)
                updateDesktop()
                return
            }
        }

        vm.incLoading(1)
        Http.getString("driver/myself")
            .then { s ->
                mModel = JDriver.parse(s)
                if (mModel != null) {
                    HardCache.put("driver.model", s!!, 60)
                    updateDesktop()
                } else {
                    HardCache.put("driver.model", "", -1)

                    val msg = "Pelo visto você não é motorista. Crie uma requisição na tela de perfil do usuário."
                    D.ok(requireContext(), msg, "Motorista", "Cancelar") {
                        MainActivity.shared?.outNavigateProfile()
                    }
                }
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.incLoading(-1)
            }
    }

    private val permissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
        var allPermitted = true
        permissions.entries.forEach {
            if ( ! it.value) allPermitted = false
        }
        if (allPermitted) {
            locateItSelf()
        } else {
            mAuthorized.value = false
            DialogMini.error(this, "É necessário sua localização para melhor aproveitar nosso app!")
        }
    }

    private var mDialogAsk: AlertDialog? = null
    private fun locateInit()
    {
        mLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        if (
            ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
//            && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED
        )
        {
            mAuthorized.value = false

            mDialogAsk = AlertDialog.Builder(requireContext()).setTitle("Permissão")
                .setMessage("Para o funcionamento correto do sistema e melhor atendê-lo, gostariamos de saber sua localização. Assim o sistema saberá quais anúncios e quais passageiros recomendar para você.")
                .setPositiveButton("Ok") { _, _ ->

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        val permissions = arrayListOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                            permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        }
                        permissionLauncher.launch(permissions.toTypedArray())
                    }
                }
                .setNegativeButton("Cancelar") { dialog, _ -> dialog?.dismiss() }
                .show()

        } else {
            mAuthorized.value = true
        }
    }

    @SuppressLint("MissingPermission")
    private fun locateItSelf()
    {
        mLocationClient.lastLocation.addOnSuccessListener {
            ui.mapView.getMapAsync { g ->

                g.isMyLocationEnabled = true

                if (it===null) return@getMapAsync

                val latLng = LatLng(it.latitude, it.longitude)
                g.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

            }
        }
    }

    fun outRequestAd()
    {
        if (App.user.value == null) return
        Task.debounce("FragDriver@outRequestAd", 1000) {

            vm.incLoading(1)
            Http.getString("ad/driver")
                .then { s ->
                    val list = JAd.parseList(s)
                    mAdapter.update(list)
                    DialogDriverAd.validateInList(list)
                }
                .catch {
                    DialogMini.error(this, it)
                }
                .always {
                    vm.incLoading(-1)
                }

        }
    }


    private var mLocateItSelfStart = false
    override fun onStart()
    {
        super.onStart()
        shared = this
        Repo.userPref.putDrawerStartDestination("driver")

        // service
        if (App.user.value != null) LocationService.startAndBind(requireActivity(), mServiceConn)

        ui.mapView.onStart()

        locateInit()

        if ( ! mLocateItSelfStart) {
            mLocateItSelfStart = true
            locateItSelf()
        }

        requestMyself(false)
        outRequestAd()

        DialogDriverAd.attach(this)

    }

    override fun onStop()
    {
        super.onStop()
        DialogDriverAd.detach(this)

        if (App.user.value != null) LocationService.unbind(requireActivity(), mServiceConn)

        ui.mapView.onStop()

        shared = null
        mDialogAsk?.dismiss()
        mDialogAsk = null
    }

    override fun onDestroy() {
        super.onDestroy()
        ui.mapView.onDestroy()
        _ui = null
    }

}