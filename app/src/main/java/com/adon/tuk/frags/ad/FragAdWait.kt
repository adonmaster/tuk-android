package com.adon.tuk.frags.ad

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.AdVM
import com.adon.tuk.R
import com.adon.tuk.databinding.FragAdWaitBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.frags.ad.adapters.RvFragAdWaitAdapter
import com.adon.tuk.helpers.Geo
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.HttpFormData
import com.adon.tuk.helpers.Task
import com.adon.tuk.jmodels.JAd
import com.adon.tuk.jmodels.JProposal
import com.adon.tuk.repo.Repo
import com.adon.tuk.structs.GeoPath
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import org.json.JSONObject
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime

class FragAdWait: Fragment() {

    companion object {
        var shared: FragAdWait? = null
    }

    //
    private lateinit var vm: AdVM
    private var _ui: FragAdWaitBinding? = null
    private val ui get() = _ui!!

    //
    private lateinit var mAdapter: RvFragAdWaitAdapter

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        vm = ViewModelProvider(this)[AdVM::class.java]
        _ui = FragAdWaitBinding.inflate(inflater, container, false)

        ui.mapView.onCreate(savedInstanceState)
        ui.mapView.alpha = .01f

        //
        mAdapter = RvFragAdWaitAdapter(object : RvFragAdWaitAdapter.Listener {
            override fun onEmpty(option: Boolean) {
                ui.wrapperEmpty.isVisible = option
            }
            override fun onSelect(model: JProposal, position: Int) {

            }
        })
        ui.rv.adapter = mAdapter
        ui.rv.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        ui.rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down)

        //
        vm.loading.observe(viewLifecycleOwner) {
            ui.progress.isVisible = it
            ui.btnCancel.isEnabled = !it
        }

        return ui.root
    }

    private fun updateDesktop(model: JAd) {

        ui.mapView.getMapAsync { g ->

            g.uiSettings.setAllGesturesEnabled(false)
            g.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style_silver_2))
            ui.mapView.animate().alpha(1f).setStartDelay(1000).start()

            g.clear()

            model.route?.let { route ->
                val bounds = GeoPath.generateBounds(route)
                g.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 55), 10, object: GoogleMap.CancelableCallback {
                    override fun onCancel() {}
                    override fun onFinish() {
                        Geo.routeLoadPath(g, route)
                    }
                })
            }

            //
            val origin = LatLng(model.origin_lat, model.origin_lng)
            val destination = LatLng(model.destination_lat, model.destination_lng)

            g.addMarker(MarkerOptions().title("Partida").position(origin))
                ?.showInfoWindow()
            g.addMarker(MarkerOptions().title("Chegada").position(destination))
        }

        // chronometer
        Task.main(500) {
            val durationBetween = Duration.between(model.created_at, LocalDateTime.now()).toMillis()
            ui.timer.base = SystemClock.elapsedRealtime() - durationBetween
            ui.timer.start()
        }

        ui.lblWaiting.text = "[${model.id}] Aguardando motoristas..."

        ui.btnCancel.setOnClickListener {

            vm.loading.value = true
            Http.delete("ad", HttpFormData().put("ad_id", model.id))
                .then {
                    Repo.userPref.putStatus(null)
                    requireActivity().finish()
                }
                .catch {
                    DialogMini.error(this, it)
                }
                .always {
                    vm.loading.value = false
                }

        }

        //
        requestProposals(model)
    }

    private fun requestProposals(model: JAd)
    {
        vm.loading.value = true
        Http.getString("ad/${model.id}/proposal")
            .then {
                mAdapter.update(JProposal.parseList(it))
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    public fun outRequestAd() = requestAd()

    private fun requestAd()
    {
        vm.loading.value = true
        Http.getString("ad")
            .then {
                if (it?.isNotBlank() == true) {
                    val model = JAd.parse(JSONObject(it))
                    vm.ad = model
                    updateDesktop(model)
                } else {
                    vm.ad = null
                    Repo.userPref.putStatus(null)
                    requireActivity().finish()
                }
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    fun outRequestProposal() {
        vm.ad?.let { requestProposals(it) }
    }

    override fun onStart() {
        super.onStart()
        shared = this

        vm.ad?.let { updateDesktop(it) }
        if (vm.ad == null) requestAd()

        ui.mapView.onStart()

    }

    override fun onStop() {
        super.onStop()
        shared = null

        ui.mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        ui.mapView.onDestroy()
        _ui = null
    }
}