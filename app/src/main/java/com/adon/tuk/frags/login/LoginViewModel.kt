package com.adon.tuk.frags.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel: ViewModel() {

    var email = ""
    var name = ""

    val loading = MutableLiveData<Boolean>().also { it.value = false }

}