package com.adon.tuk.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.adon.tuk.R
import com.adon.tuk.databinding.FragProfileDriverPendingBinding
import com.adon.tuk.repo.Repo

class FragProfileDriverPending: Fragment() {

    //
    private var _ui: FragProfileDriverPendingBinding? = null
    private val ui get() = _ui!!

    //
    private lateinit var vmRequest: FragProfileDriverRequestVM

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vmRequest = ViewModelProvider(requireActivity())[FragProfileDriverRequestVM::class.java]
        _ui = FragProfileDriverPendingBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = Repo.userPref.get("driver_request_id", -1)
        ui.lblDriverRequisition.text = "Requisição n.o $id"

        ui.btnSubmit.setOnClickListener {
            vmRequest.reset()
            FragProfileDriver.shared.outNavigateToRequest()
        }
    }

}