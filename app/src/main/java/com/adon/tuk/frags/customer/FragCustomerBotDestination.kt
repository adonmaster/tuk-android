package com.adon.tuk.frags.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.R
import com.adon.tuk.databinding.FragCustomerBotDestinationBinding
import com.adon.tuk.helpers.Span

class FragCustomerBotDestination: Fragment() {

    //
    private lateinit var vm: FragCustomerVM
    private var _ui: FragCustomerBotDestinationBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        vm = ViewModelProvider(requireActivity())[FragCustomerVM::class.java]
        _ui = FragCustomerBotDestinationBinding.inflate(inflater, container, false)


        vm.destinationAddress.observe(viewLifecycleOwner) {
            val address = it ?: "Carregando..."
            val position = vm.destination.value?.let { p -> " | ${p.latitude},${p.longitude}" } ?: ""
            ui.lblDesc.text = Span(address)
                .color(requireContext(), R.color.md_grey_600).bold()
                .concat(position)
                .color(requireContext(), R.color.md_grey_500).styleNormal()
                .build()
        }

        ui.btnNext.setOnClickListener {
            FragCustomer.shared?.outSubmitDestination(false)
        }

        return ui.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }
}