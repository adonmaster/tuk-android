package com.adon.tuk.frags.customer

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.R
import com.adon.tuk.databinding.FragCustomerBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.frags.BaseFragMapView
import com.adon.tuk.helpers.Act
import com.adon.tuk.helpers.Geo
import com.adon.tuk.helpers.Task
import com.adon.tuk.helpers.ter
import com.adon.tuk.repo.Repo
import com.adon.tuk.structs.GeoPath
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class FragCustomer : BaseFragMapView() {

    companion object {
        var shared: FragCustomer? = null
    }

    //
    private lateinit var vm: FragCustomerVM
    private var _ui: FragCustomerBinding? = null
    private val ui get() = _ui!!

    //
    private val mLocationClient by lazy {
        LocationServices.getFusedLocationProviderClient(
            requireActivity()
        )
    }

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vm = ViewModelProvider(requireActivity())[FragCustomerVM::class.java]
        _ui = FragCustomerBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shared = this
        ui.mapView.onCreate(savedInstanceState)

        // places
        val apiKey = requireActivity().getString(R.string.geo_google_api_key)
        if (!Places.isInitialized()) Places.initialize(requireContext(), apiKey)
        val fragPlace =
            childFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as AutocompleteSupportFragment
        fragPlace.setPlaceFields(listOf(Place.Field.LAT_LNG, Place.Field.ADDRESS))
        fragPlace.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onError(p0: Status) {
                if (!p0.isCanceled) DialogMini.error(
                    this@FragCustomer,
                    p0.statusMessage ?: "Erro ao carregar localização"
                )
            }

            override fun onPlaceSelected(p0: Place) {
                placeSelected(p0)
            }
        })

        //
        val hasDestination = vm.destination.value != null
        ui.wrapperBottom.isVisible = hasDestination
        vm.page.value = FragCustomerVM.Page.DESTINATION
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotDestination::class.java
        )

        //
        vm.page.observe(viewLifecycleOwner) { page ->

            ui.imgMarker.animate().alpha(0f).start()

            ui.mapView.getMapAsync { g ->
                val position = when (page) {
                    FragCustomerVM.Page.ORIGIN -> vm.origin.value
                    FragCustomerVM.Page.DESTINATION -> vm.destination.value
                    else -> null
                } ?: return@getMapAsync

                ui.imgMarker.animate().alpha(1f).start()

                g.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17f))
            }
        }

        //
        vm.loading.observe(viewLifecycleOwner) {
            ui.progress.isVisible = it
        }
    }

    var mOriginPristine = false
    var mDestinationPristine = false
    private fun placeSelected(place: Place) {
        val position = place.latLng ?: return
        val address = place.address ?: "Brasil"
        var showMarker = false

        if (vm.page.value == FragCustomerVM.Page.ORIGIN) {
            vm.origin.value = position
            vm.originAddress.value = address
            showMarker = true
            mOriginPristine = true
        } else if (vm.page.value == FragCustomerVM.Page.DESTINATION) {
            ui.wrapperBottom.isVisible = true
            showMarker = true
            vm.destination.value = position
            vm.destinationAddress.value = address
            mDestinationPristine = true
        }

        ui.imgMarker.animate().setStartDelay(900).alpha(ter(showMarker, 1f, 0f)).start()

        ui.mapView.getMapAsync { g ->
            g.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17f))
        }
    }

    fun outSubmitDestination(reverseAnimation: Boolean) {
        if (!mDestinationPristine) {
            ui.mapView.getMapAsync { g ->
                val location = g.projection.visibleRegion.latLngBounds.center
                vm.destination.value = location
                runBlocking {
                    vm.destinationAddress.value = withContext(Dispatchers.IO) {
                        Geo.addressLineFrom(requireContext(), location)
                    }
                }
            }
        }

        vm.page.value = FragCustomerVM.Page.ORIGIN
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotOrigin::class.java,
            null,
            reverseAnimation
        )
    }

    fun outSubmitOrigin() {
        if (!mOriginPristine) {
            ui.mapView.getMapAsync { g ->
                val location = g.projection.visibleRegion.latLngBounds.center
                vm.origin.value = location
                runBlocking {
                    vm.originAddress.value = withContext(Dispatchers.IO) {
                        Geo.addressLineFrom(requireContext(), location)
                    }
                }
            }
        }

        vm.page.value = FragCustomerVM.Page.ROUTE
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotRoute::class.java
        )
    }

    fun outCancelOrigin() {
        if (!mOriginPristine) {
            ui.mapView.getMapAsync { g ->
                val location = g.projection.visibleRegion.latLngBounds.center
                vm.origin.value = location
                runBlocking {
                    vm.originAddress.value = withContext(Dispatchers.IO) {
                        Geo.addressLineFrom(requireContext(), location)
                    }
                }
            }
        }

        vm.page.value = FragCustomerVM.Page.DESTINATION
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotDestination::class.java,
            null,
            true
        )
    }

    fun outCancelRoute() {
        ui.mapView.getMapAsync { g -> g.clear() }

        vm.page.value = FragCustomerVM.Page.ORIGIN
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotOrigin::class.java,
            null,
            true
        )
    }


    fun outSubmittedRoute() {
        ui.mapView.getMapAsync { g -> g.clear() }

        vm.page.value = FragCustomerVM.Page.DESTINATION
        Act.fragReplace(
            childFragmentManager,
            R.id.frag_container_bot,
            FragCustomerBotDestination::class.java,
            null,
            true
        )
    }

    fun outRouteResolved(route: List<List<LatLng>>) {
        ui.mapView.getMapAsync { g ->

            g.clear()

            g.addMarker(MarkerOptions().position(vm.origin.value!!).title("Partida"))
                ?.showInfoWindow()

            g.addMarker(MarkerOptions().position(vm.destination.value!!).title("Chegada"))

            val bounds = GeoPath.generateBounds(route)
            g.animateCamera(
                CameraUpdateFactory.newLatLngBounds(bounds.build(), 100),
                object : GoogleMap.CancelableCallback {
                    override fun onCancel() {}
                    override fun onFinish() {
                        Geo.routeLoadPath(g, route)
                    }
                })


        }
    }


    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            var allPermitted = true
            permissions.entries.forEach {
                if (!it.value) allPermitted = false
            }
            if (allPermitted) {
                locateStTime()
            } else {
                DialogMini.error(
                    this,
                    "É necessário sua localização para melhor aproveitar nosso app!"
                )
            }
        }

    private var mDialogAsk: AlertDialog? = null
    private fun locateInit() {

        if (
            ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            mDialogAsk = AlertDialog.Builder(requireContext()).setTitle("Permissão")
                .setMessage("Para o funcionamento correto do sistema e melhor atendê-lo, gostariamos de saber sua localização. Assim o motorista saberá como e onde começará a viagem.")
                .setPositiveButton("Ok") { _, _ ->

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        permissionLauncher.launch(
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            )
                        )
                    }

                }
                .setNegativeButton("Cancelar") { dialog, _ -> dialog?.dismiss() }
                .show()
        }
    }

    private var mDraggingMap = false

    @SuppressLint("MissingPermission")
    private fun locateStTime() {
        if (vm.origin.value != null) return

        mLocationClient.lastLocation.addOnSuccessListener {
            ui.mapView.getMapAsync { g ->

                g.isMyLocationEnabled = true
                g.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        requireContext(),
                        R.raw.map_style_silver_2
                    )
                )
                g.setOnCameraMoveStartedListener {
                    mDraggingMap = it == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
                }
                g.setOnCameraIdleListener {
                    ui.imgMarker.animate()
                        .setStartDelay(700)
                        .rotationY(ter(ui.imgMarker.rotationY == 360f, 0f, 360f))
                        .start()

                    if (!mDraggingMap) return@setOnCameraIdleListener

                    if (vm.page.value == FragCustomerVM.Page.DESTINATION) mDestinationPristine =
                        false
                    if (vm.page.value == FragCustomerVM.Page.ORIGIN) mOriginPristine = false
                }

                val position = LatLng(it.latitude, it.longitude)
                g.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15f))

                vm.origin.value = position
                runBlocking {
                    vm.originAddress.value = withContext(Dispatchers.IO) {
                        Geo.addressLineFrom(requireContext(), position)
                    }
                }

            }
        }
    }

    override fun onStart() {
        super.onStart()
        Repo.userPref.putDrawerStartDestination("customer")
        ui.mapView.onStart()

        locateInit()
        locateStTime()
    }

    override fun onStop() {
        super.onStop()
        ui.mapView.onStop()

        mDialogAsk?.dismiss()
        mDialogAsk = null
    }

    override fun onDestroy() {
        super.onDestroy()
        shared = null
        ui.mapView.onDestroy()
        _ui = null
    }


}