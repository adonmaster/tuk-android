package com.adon.tuk.frags.driver_request

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.BuildConfig
import com.adon.tuk.DriverRequestVM
import com.adon.tuk.databinding.FragDriverRequestLicenseBinding
import com.adon.tuk.helpers.*
import java.io.File

class FragDriverRequestLicense: Fragment() {

    //
    private var _ui: FragDriverRequestLicenseBinding? = null
    private val ui get() = _ui!!
    private lateinit var vm: DriverRequestVM

    //
    private lateinit var launcherTakePic: ActivityResultLauncher<Uri>
    private lateinit var mImgTmpFile: Uri

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragDriverRequestLicenseBinding.inflate(inflater, container, false)

        //
        vm = ViewModelProvider(requireActivity())[DriverRequestVM::class.java]

        //
        ui.edNo.setText(vm.license["no"])
        ui.edNo.addTextChangedListener { vm.licensePut("no", it.toString()) }

        //
        ui.edValidUntil.setText(vm.license["valid"])
        MaskWatcher.bind(ui.edValidUntil, "##/##/####")
        ui.edValidUntil.addTextChangedListener { vm.licensePut("valid", it.toString()) }

        //
        launcherTakePic = registerForActivityResult(ActivityResultContracts.TakePicture()) {
            if (it) {
                val f = ImgCorrector.cameraUriToInternalFile(requireActivity(), mImgTmpFile, "frag_driver_request_license", "doc.png")

                vm.loading.value = true
                HttpMedia.upload(f, HttpMulti.ContentType.PNG)
                    .then { j ->
                        vm.licensePut("img", j.full)
                        vm.licensePut("img_uid", j.uid)
                        Img.load(j.full, ui.imgDocument)
                        ui.imgDocument.alpha = 1f
                    }
                    .catch {
                        T.error(requireActivity(), it)
                    }
                    .always {
                        vm.loading.value = false
                    }
            }
        }
        ui.imgDocument.setOnClickListener {
            initAvatarTmpFile()
            launcherTakePic.launch(mImgTmpFile)
        }


        return ui.root
    }

    private fun initAvatarTmpFile() {
        val f = File.createTempFile("tmp_img_file_2", ".png", requireActivity().cacheDir)
        f.createNewFile()
        f.deleteOnExit()
        mImgTmpFile = FileProvider.getUriForFile(requireActivity().applicationContext, "${BuildConfig.APPLICATION_ID}.fileprovider", f)
    }

    override fun onResume() {
        super.onResume()

        val imgUrl = vm.detail["img"]
        if (imgUrl?.isNotBlank() == true) {
            Img.load(imgUrl, ui.imgDocument)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}