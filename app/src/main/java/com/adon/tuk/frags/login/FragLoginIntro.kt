package com.adon.tuk.frags.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.adon.tuk.Env
import com.adon.tuk.R
import com.adon.tuk.databinding.FragLoginIntroBinding
import com.adon.tuk.helpers.Task

class FragLoginIntro: Fragment() {

    //
    private var _binding: FragLoginIntroBinding? = null
    private val binding get() = _binding!!

    //

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragLoginIntroBinding.inflate(inflater, container, false)

        binding.uiVersion.text = "Versão ${Env.versionName()}"

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        Task.main(2500) {
            findNavController().navigate(R.id.sg_to_login_email)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}