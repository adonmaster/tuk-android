package com.adon.tuk.frags.driver

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.jmodels.JDriver

class FragDriverVM: ViewModel() {

    val loading = MutableLiveData(0)

    fun incLoading(factor: Int) {
        loading.value = loading.value!! + factor
    }
}