package com.adon.tuk.frags.driver.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.R
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JAd
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.*
import org.threeten.bp.LocalDateTime
import kotlin.math.min

@DelicateCoroutinesApi
class RvFragDriverAdapter(val listener: Listener)
    : RecyclerView.Adapter<RvFragDriverAdapter.VH>()
{

    private val _data = RvData<JAd, RvFragDriverAdapter.VH>(this)
    val data get() = _data.items()

    operator fun get(position: Int?): JAd? {
        return position?.let { data.getOrNull(it) }
    }

    private val _selected = FlowEx(-1)
    val selected = _selected.toObserver()

    override fun getItemCount(): Int {
        listener.onEmpty(data.isEmpty())
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return data[position].id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_driver, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = data[position]
        val selected = _selected.v == position
        val alpha = ter(selected, 1f, .2f)

        holder.avatar.alpha = alpha
        holder.avatar.borderColor = Cl.ter(selected, R.color.md_grey_700, R.color.transparent)
        Img.loadAvatar(m.user?.avatar_thumb, holder.avatar)

        holder.title.text = m.user_name
        holder.title.alpha = alpha

        holder.badge.alpha = alpha

    }

    fun update(data: List<JAd>) {
        _data.update(data)
        setSelected(ter(_selected.v == -1, 0, _selected.v))
    }

    private fun setSelected(_new: Int?) {
        val old = _selected.v
        val new = min(_new ?: _selected.v, data.size-1)
        Rv.notifyChanged(this, old, new)
        _selected.emit(new)
    }


    //
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatar: CircleImageView = itemView.findViewById(R.id.avatar)
        val title: TextView = itemView.findViewById(R.id.lbl_title)
        val badge: TextView = itemView.findViewById(R.id.lbl_badge)
        init {
            avatar.setOnClickListener {
                setSelected(absoluteAdapterPosition)
                listener.onSelected(data[absoluteAdapterPosition])
            }
        }
    }

    interface Listener {
        fun onSelected(item: JAd)
        fun onEmpty(option: Boolean)
    }
}