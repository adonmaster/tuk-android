package com.adon.tuk.frags.ad

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.R
import com.adon.tuk.databinding.FragAdRequestStoreBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JPostAd
import com.adon.tuk.repo.Repo
import com.adon.tuk.structs.AdRequestStruct
import com.adon.tuk.structs.GeoPath

class FragAdRequestStore: Fragment() {

    //
    private lateinit var vm: FragAdRequestVM
    private var _ui: FragAdRequestStoreBinding? = null
    private val ui get() = _ui!!

    private val cRequest by lazy {  requireArguments().getParcelable<AdRequestStruct>("request")!! }


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
       /* vm = ViewModelProvider(requireActivity())[FragAdRequestVM::class.java]
        _ui = FragAdRequestStoreBinding.inflate(inflater, container, false)

        vm.value.value = vm.valueSuggested.toDouble()

        vm.value.observe(viewLifecycleOwner) {
            ui.lblValue.text = "R$ ${vm.valueCurrency}"
        }

        ui.btnValueMinus.setOnClickListener {
            vm.valueInc(-1)
        }
        ui.btnValueMinus5.setOnClickListener {
            vm.valueInc(-5)
        }

        ui.btnValuePlus.setOnClickListener {
            vm.valueInc(1)
        }
        ui.btnValuePlus5.setOnClickListener {
            vm.valueInc(5)
        }

        vm.distance.observe(viewLifecycleOwner) { d ->
            ui.lblSuggestion.text = Span("Rota de ")
                .concat(vm.distanceStr.value!!)
                .color(R.color.md_indigo_500).boldItalic()
                .concat(", sugerimos um valor de ")
                .concat("R$ ${vm.valueSuggestedCurrency}")
                .color(R.color.md_indigo_500).boldItalic()
                .build()
        }

        ui.btnCancel.setOnClickListener {
            requireActivity().finish()
        }

        ui.btnSubmit.setOnClickListener {
            vm.loading.value = true

            val params = HttpFormData()
                .put("origin_lat", cRequest.origin.latitude)
                .put("origin_lng", cRequest.origin.longitude)
                .put("origin_s", cRequest.originLine)

                .put("destination_lat", cRequest.destination.latitude)
                .put("destination_lng", cRequest.destination.longitude)
                .put("destination_s", cRequest.destinationLine)

                .put("route", GeoPath.toJsonArray(cRequest.routePath))
                .put("duration", cRequest.routeDuration)
                .put("duration_s", cRequest.routeDurationStr)
                .put("distance", cRequest.routeDistance)
                .put("distance_s", cRequest.routeDistanceStr)
                .put("price", vm.value.value!!)

            Http.post("ad", params)
                .then { _ ->

                }
                .catch {
                    DialogMini.error(childFragmentManager, it)
                }
                .always {
                    vm.loading.value = false
                }
        }

        vm.loading.observe(viewLifecycleOwner) {
            ui.progress.isVisible = it
            ui.btnSubmit.isEnabled = !it
            ui.btnCancel.isEnabled = !it
        }
*/
        return ui.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}