package com.adon.tuk.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.adon.tuk.databinding.FragProfileBinding
import com.adon.tuk.helpers.ter
import com.google.android.material.tabs.TabLayoutMediator

class FragProfile: Fragment() {

    //
    private var _binding: FragProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var vm: FragProfileVM


    //
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProvider(requireActivity())[FragProfileVM::class.java]
        vm.loading.observe(viewLifecycleOwner) {
            binding.progress.visibility = ter(it, View.VISIBLE, View.GONE)
        }

        setupPager()
    }

    private fun setupPager()
    {
        val pages = arrayOf(
            "Perfil" to { FragProfileDetail() },
            "Motorista" to { FragProfileDriver() }
        )

        binding.pager.adapter = object: FragmentStateAdapter(requireActivity()) {
            override fun getItemCount(): Int {
                return pages.size
            }
            override fun createFragment(position: Int): Fragment {
                return pages[position].second()
            }
        }

        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = pages[position].first
        }.attach()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}