package com.adon.tuk.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.FragProfileDriverRequestBinding

class FragProfileDriverRequest: Fragment() {

    //
    private var _ui: FragProfileDriverRequestBinding? = null
    private val ui get() = _ui!!

    //

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragProfileDriverRequestBinding.inflate(inflater, container, false)

        //
        ui.btnRequest.setOnClickListener {
            FragProfileDriver.shared.outLaunchDriverRequest()
        }

        return ui.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}