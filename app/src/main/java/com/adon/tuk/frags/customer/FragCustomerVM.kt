package com.adon.tuk.frags.customer

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adon.tuk.structs.GeoRoute
import com.google.android.gms.maps.model.LatLng


class FragCustomerVM: ViewModel() {

    val loading = MutableLiveData(false)

    enum class Page { DESTINATION, ORIGIN, ROUTE }
    val page = MutableLiveData(Page.DESTINATION)

    val origin = MutableLiveData<LatLng>(null)
    val destination = MutableLiveData<LatLng>(null)

    val originAddress = MutableLiveData<String>(null)
    val destinationAddress = MutableLiveData<String>(null)

    val route = MutableLiveData<List<List<LatLng>>>(null)

    val duration = MutableLiveData<Int>(null)
    val durationStr = MutableLiveData<String>(null)

    val distance = MutableLiveData<Int>(null)
    val distanceStr = MutableLiveData<String>(null)

}