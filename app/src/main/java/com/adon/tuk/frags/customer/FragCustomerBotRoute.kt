package com.adon.tuk.frags.customer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.AdActivity
import com.adon.tuk.databinding.FragCustomerBotRouteBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.helpers.*
import com.adon.tuk.structs.AdRequestStruct
import com.adon.tuk.structs.GeoDistance

class FragCustomerBotRoute: Fragment() {

    //
    private lateinit var vm: FragCustomerVM
    private var _ui: FragCustomerBotRouteBinding? = null
    private val ui get() = _ui!!


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        vm = ViewModelProvider(requireActivity())[FragCustomerVM::class.java]
        _ui = FragCustomerBotRouteBinding.inflate(inflater, container, false)

        ui.btnPrior.setOnClickListener {
            FragCustomer.shared?.outCancelRoute()
        }

        ui.btnNext.setOnClickListener {

            if ( ! grant(
                vm.origin.value, vm.originAddress.value, vm.destination.value, vm.destinationAddress.value,
                vm.route.value, vm.distance.value, vm.duration.value
            )) return@setOnClickListener

            AdActivity.startRequest(requireActivity(), AdRequestStruct(
                vm.origin.value!!, vm.originAddress.value!!,
                vm.destination.value!!, vm.destinationAddress.value!!,
                vm.route.value!!,
                vm.durationStr.value!!, vm.duration.value!!,
                vm.distanceStr.value!!, vm.distance.value!!
            ))

            FragCustomer.shared?.outSubmittedRoute()

        }

        vm.loading.observe(viewLifecycleOwner) {
            ui.btnNext.isEnabled = !it
            ui.btnPrior.isEnabled = !it
        }

        return ui.root
    }

    private fun updateDesktop() {
        ui.lblDesc.text = "Distância: ${vm.distanceStr.value}\nDuração: ${vm.durationStr.value}"
    }

    private fun requestRoute()
    {
        val origin = vm.origin.value ?: return
        val destination = vm.destination.value ?: return

        vm.loading.value = true
        Geo.requestFull(origin, destination)
            .then { model ->
                vm.destinationAddress.value = model.destination
                vm.originAddress.value = model.origin

                vm.distance.value = model.distance
                vm.distanceStr.value = model.distanceStr

                vm.duration.value = model.duration
                vm.durationStr.value = model.durationStr

                vm.route.value = model.route

                updateDesktop()
                FragCustomer.shared?.outRouteResolved(model.route)
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    override fun onStart() {
        super.onStart()

        requestRoute()
    }


    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}

