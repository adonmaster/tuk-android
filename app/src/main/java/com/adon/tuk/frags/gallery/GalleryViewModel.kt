package com.adon.tuk.frags.gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GalleryViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Aqui pretendo colocar Configurações, aguarde novas versões!"
    }
    val text: LiveData<String> = _text
}