package com.adon.tuk.frags.ad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adon.tuk.AdActivity
import com.adon.tuk.R
import com.adon.tuk.databinding.FragAdRequestWaitBinding
import com.adon.tuk.dialogs.DialogAdDriverHire
import com.adon.tuk.helpers.Task
import com.adon.tuk.rmodels.DriverDummyRModel
import com.adon.tuk.frags.ad.adapters.FragAdRequestWaitAdapter
import org.threeten.bp.LocalDate
import java.util.*


class FragAdRequestWait: Fragment(), DialogAdDriverHire.Listener {

    //
    private var _ui: FragAdRequestWaitBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        _ui = FragAdRequestWaitBinding.inflate(inflater, container, false)

        ui.timer.start()

        ui.btnCancel.setOnClickListener {
            requireActivity().finish()
        }

        val adapter = FragAdRequestWaitAdapter(object : FragAdRequestWaitAdapter.Listener {
            override fun onSelect(model: DriverDummyRModel, position: Int) {
                DialogAdDriverHire.show(childFragmentManager, model)
            }
        })
        ui.rv.adapter = adapter
        ui.rv.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        ui.rv.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        ui.rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down)

        //
        Task.main(3000) {
            val id = Random().nextInt()
            adapter.data.add(0, DriverDummyRModel(
                id, "https://static.wikia.nocookie.net/fantastico/images/e/e5/Rowan-Atkinson-Mr.-Bean.jpg/revision/latest/scale-to-width-down/515?cb=20140429015338&path-prefix=pt-br",
                "Carlos Beicinho",
                5, 25, 2, 45.0, LocalDate.now().minusDays(200)
            ))
            adapter.notifyItemInserted(0)
        }

        Task.main(5000) {
            val id = Random().nextInt()
            adapter.data.add(0, DriverDummyRModel(
                id, "https://gravatar.com/avatar/d0491e517ea52471facccbefa4f17c2a?s=400&d=robohash&r=x",
                "Ronaldo Beicinho",
                15, 50, 50, 32.0, LocalDate.now().minusDays(100)
            ))
            adapter.notifyItemInserted(0)
        }

        Task.main(9000) {
            val id = Random().nextInt()
            adapter.data.add(0, DriverDummyRModel(
                id, "https://aaronturatv.ig.com.br/wp-content/uploads/2021/07/silvio-santos.jpg",
                "Finalmente Beicinho",
                10, 5, 8, 30.0, LocalDate.now().minusDays(20)
            ))
            adapter.notifyItemInserted(0)
        }

        return ui.root
    }

    override fun dialogAdDriverHireOnConfirm(model: DriverDummyRModel) {
        AdActivity.shared?.outNavigateToDriverConfirm(model)
    }

    //
    override fun onStart() {
        super.onStart()
        DialogAdDriverHire.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogAdDriverHire.detach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}