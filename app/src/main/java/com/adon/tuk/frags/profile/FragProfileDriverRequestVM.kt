package com.adon.tuk.frags.profile

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FragProfileDriverRequestVM: ViewModel() {

    val loading = MutableLiveData<Boolean>(false)

    val name = MutableLiveData<String?>(null)
    val city = MutableLiveData<String?>(null)
    val gender = MutableLiveData<String?>(null)

    val cnhFront = MutableLiveData<Uri?>(null)
    val cnhBack = MutableLiveData<Uri?>(null)

    fun reset() {
        name.value = null
        city.value = null
        gender.value = null
        cnhFront.value = null
        cnhBack.value = null
    }

}