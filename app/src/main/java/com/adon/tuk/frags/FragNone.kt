package com.adon.tuk.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.FragNoneBinding

class FragNone: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        return FragNoneBinding.inflate(inflater, container, false).root
    }
}