package com.adon.tuk.frags.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.adon.tuk.App
import com.adon.tuk.R
import com.adon.tuk.databinding.FragLoginCodeBinding
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.Span
import com.adon.tuk.helpers.T
import com.adon.tuk.repo.Repo
import com.adon.tuk.structs.SimpleTextChangedListener
import com.adon.tuk.jmodels.JRegisterCode
import org.json.JSONObject

class FragLoginCode: Fragment() {

    //
    private lateinit var vm: LoginViewModel

    //
    private var _ui: FragLoginCodeBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        //
        vm = ViewModelProvider(requireActivity())[LoginViewModel::class.java]
        _ui = FragLoginCodeBinding.inflate(inflater, container, false)

        //
        ui.uiLblDesc.text = Span("Enviamos um email para\n")
            .concat(vm.email).bold().color(requireContext(), R.color.md_blue_500)
            .concat("!\nPor favor, confirme o código enviado:")
            .build()

        ui.uiEdCode.addTextChangedListener(object: SimpleTextChangedListener() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ui.uiBtnSubmit.isEnabled = s?.trim()?.isEmpty() == false
            }
        })
        ui.uiEdCode.setText("")


        vm.loading.observe(viewLifecycleOwner, Observer {
            ui.uiProgress.isVisible = it
            ui.uiBtnSubmit.isEnabled = !it
            ui.uiBtnCancel.isEnabled = !it
        })

        //
        ui.uiBtnCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        //
        ui.uiBtnSubmit.setOnClickListener {
            submit()
        }

        //
        return ui.root
    }

    private fun submit()
    {
        val params = Http.buildParams()
            .put("name", vm.name)
            .put("email", vm.email)
            .put("code", ui.uiEdCode.text.toString())

        vm.loading.value = true
        Http.post("user/register/check", params)
            .then {
                afterSubmit(it)
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    private fun afterSubmit(json: JSONObject)
    {
        // db
        val jmodel = JRegisterCode.parse(json)
        Repo.user.registerFrom(jmodel)

        // ui
        App.shared.userLoad()

        activity?.finish()
    }

    //
    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }
}