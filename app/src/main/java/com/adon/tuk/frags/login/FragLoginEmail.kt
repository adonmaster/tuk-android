package com.adon.tuk.frags.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.databinding.FragLoginEmailBinding
import com.adon.tuk.helpers.Http
import com.adon.tuk.helpers.HttpFormData
import com.adon.tuk.helpers.T
import androidx.navigation.fragment.findNavController
import com.adon.tuk.R


class FragLoginEmail: Fragment() {

    //
    private lateinit var vm: LoginViewModel

    //
    private var _ui: FragLoginEmailBinding? = null
    private val ui get() = _ui!!

    //

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        //
        vm = ViewModelProvider(requireActivity())[LoginViewModel::class.java]
        _ui = FragLoginEmailBinding.inflate(inflater, container, false)

        vm.loading.observe(viewLifecycleOwner, Observer {
            ui.uiProgress.isVisible = it
            ui.uiBtnSubmit.isEnabled = !it
        })

        ui.uiEdEmail.setText(vm.email)
        ui.uiEdName.setText(vm.name)

        ui.uiBtnSubmit.setOnClickListener {
            trySubmit()
        }

        //
        return ui.root
    }

    //

    private fun trySubmit()
    {
        vm.email = ui.uiEdEmail.text.toString()
        vm.name = ui.uiEdName.text.toString()

        // validation
        if (vm.name.isBlank()) {
            T.error(activity, "Preencha corretamente o nome.")
            return
        }

        if (vm.email.isBlank()) {
            T.error(activity, "Preencha corretamente o email")
            return
        }

        submit()
    }

    private fun submit()
    {
        val params = HttpFormData()
            .put("name", vm.name)
            .put("email", vm.email)

        vm.loading.value = true
        Http.post("user/register", params)
            .then {
                findNavController().navigate(R.id.sg_to_login_code)
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    //

    override fun onDestroyView() {
        super.onDestroyView()
        _ui = null
    }

}