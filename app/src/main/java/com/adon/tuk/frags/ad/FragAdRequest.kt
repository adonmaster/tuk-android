package com.adon.tuk.frags.ad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.AdActivity
import com.adon.tuk.R
import com.adon.tuk.databinding.FragAdRequestBinding
import com.adon.tuk.dialogs.DialogMini
import com.adon.tuk.helpers.*
import com.adon.tuk.jmodels.JPostAd
import com.adon.tuk.structs.AdRequestStruct
import com.adon.tuk.structs.GeoPath
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions

class FragAdRequest: Fragment() {

    companion object {
        lateinit var shared: FragAdRequest
    }

    //
    private lateinit var vm: FragAdRequestVM
    private var _ui: FragAdRequestBinding? = null
    private val ui get() = _ui!!

    //
    private val cRequest get() = requireArguments().getParcelable<AdRequestStruct>("request")!!

    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        shared = this
    }

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        vm = ViewModelProvider(requireActivity())[FragAdRequestVM::class.java]
        _ui = FragAdRequestBinding.inflate(inflater, container, false)

        //
        ui.addressOrigin.text = cRequest.originLine
        ui.addressDestination.text = cRequest.destinationLine

        //
        ui.mapView.onCreate(savedInstanceState)
        ui.mapView.getMapAsync { g ->

            g.uiSettings.setAllGesturesEnabled(false)
            g.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style_silver_2))

            val bounds = GeoPath.generateBounds(cRequest.routePath)
            g.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 55), 10, object: GoogleMap.CancelableCallback {
                override fun onCancel() {}
                override fun onFinish() {
                    Geo.routeLoadPath(g, cRequest.routePath)
                }
            })

            //
            g.addMarker(MarkerOptions().title("Partida").position(cRequest.origin))
                ?.showInfoWindow()
            g.addMarker(MarkerOptions().title("Chegada").position(cRequest.destination))
        }

        //
        val suggestedPrice = vm.calculateSuggestedPrice(cRequest.routeDistance)
        vm.price.value = suggestedPrice
        vm.price.observe(viewLifecycleOwner) { d ->
            ui.lblPrice.text = "R$ " + Str.number(d)
        }

        ui.lblSuggestion.text = Span("Rota de ")
            .concat(cRequest.routeDistanceStr)
            .color(R.color.md_indigo_500).boldItalic()
            .concat(", sugerimos um valor de ")
            .concat("R$ ${Str.number(suggestedPrice)}")
            .color(R.color.md_indigo_500).boldItalic()
            .build()

        ui.btnValueMinus5.setOnClickListener { vm.priceInc(-5.0) }
        ui.btnValueMinus.setOnClickListener { vm.priceInc(-1.0) }
        ui.btnValuePlus.setOnClickListener { vm.priceInc(1.0) }
        ui.btnValuePlus5.setOnClickListener { vm.priceInc(5.0) }

        ui.btnSubmit.setOnClickListener { submit() }

        ui.btnCancel.setOnClickListener { requireActivity().finish() }

        vm.loading.observe(viewLifecycleOwner) {
            ui.progress.isVisible = it
            ui.btnSubmit.isEnabled = !it
            ui.btnCancel.isEnabled = !it
        }

        return ui.root
    }

    private fun submit()
    {
        vm.loading.value = true

        val params = HttpFormData()
            .put("origin_lat", cRequest.origin.latitude)
            .put("origin_lng", cRequest.origin.longitude)
            .put("origin_s", cRequest.originLine)

            .put("destination_lat", cRequest.destination.latitude)
            .put("destination_lng", cRequest.destination.longitude)
            .put("destination_s", cRequest.destinationLine)

            .put("route", GeoPath.toJsonArray(cRequest.routePath))
            .put("duration", cRequest.routeDuration)
            .put("duration_s", cRequest.routeDurationStr)
            .put("distance", cRequest.routeDistance)
            .put("distance_s", cRequest.routeDistanceStr)
            .put("price", vm.price.value!!)

        Http.post("ad", params)
            .then { json ->
                val model = JPostAd.parse(json)
                AdActivity.shared?.outNavigateToAdWait(model.payload)
            }
            .catch {
                DialogMini.error(this, it)
            }
            .always {
                vm.loading.value = false
            }
    }

    override fun onStart() {
        super.onStart()
        ui.mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        ui.mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        ui.mapView.onDestroy()
    }

}