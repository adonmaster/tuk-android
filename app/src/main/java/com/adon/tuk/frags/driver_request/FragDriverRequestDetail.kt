package com.adon.tuk.frags.driver_request

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.BuildConfig
import com.adon.tuk.DriverRequestVM
import com.adon.tuk.databinding.FragDriverRequestDetailBinding
import com.adon.tuk.dialogs.DialogCityPicker
import com.adon.tuk.helpers.*
import com.adon.tuk.models.City
import java.io.File

class FragDriverRequestDetail: Fragment(), DialogCityPicker.Listener {

    //
    private var _ui: FragDriverRequestDetailBinding? = null
    private val ui get() = _ui!!
    private lateinit var vm: DriverRequestVM

    //
    private lateinit var launcherTakePic: ActivityResultLauncher<Uri>
    private lateinit var mAvatarTmpFile: Uri


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragDriverRequestDetailBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vm = ViewModelProvider(requireActivity())[DriverRequestVM::class.java]

        // name
        ui.edNameFirst.setText(vm.detail["name_first"])
        ui.edNameFirst.addTextChangedListener { vm.detailPut("name_first", it.toString()) }
        //
        ui.edNameLast.setText(vm.detail["name_last"])
        ui.edNameLast.addTextChangedListener { vm.detailPut("name_last", it.toString()) }

        // gender
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, listOf("Masculino", "Feminino"))
        ui.edGender.setAdapter(adapter)
        ui.edGender.setText(vm.detail["gender"])
        ui.edGender.setOnItemClickListener { _, _, _, _ ->
            vm.detailPut("gender", ui.edGender.text.toString())
        }

        // birth
        ui.edBirth.setText(vm.detail["birth"])
        MaskWatcher.bind(ui.edBirth, "##/##/####")
        ui.edBirth.addTextChangedListener { vm.detailPut("birth", it.toString()) }

        //
        MaskWatcher.bindCpf(ui.edCpf)
        ui.edCpf.setText(vm.detail["cpf"])
        ui.edCpf.addTextChangedListener { vm.detailPut("cpf", it.toString()) }

        //
        ui.edRefCityUf.setText(listOfNotNull(vm.detail["ref_city"], vm.detail["ref_uf"]).joinToString(" - "))
        ui.edRefCityUf.setOnClickListener {
            DialogCityPicker.show(childFragmentManager)
        }

        //
        launcherTakePic = registerForActivityResult(ActivityResultContracts.TakePicture()) {
            if (it) {
                val f = ImgCorrector.cameraUriToInternalFile(requireActivity(), mAvatarTmpFile, "frag_driver_request_detail", "avatar.png")

                vm.loading.value = true
                HttpMedia.upload(f, HttpMulti.ContentType.PNG)
                    .then { model ->
                        vm.detailPut("avatar_uid", model.uid)
                        vm.detailPut("avatar_thumb", model.thumb)
                        vm.detailPut("avatar", model.full)

                        Img.load(model.thumb, ui.avatar)
                    }
                    .catch { reason -> T.error(requireActivity(), reason) }
                    .always {
                        vm.loading.value = false
                    }
            }
        }
        ui.avatar.setOnClickListener {
            initAvatarTmpFile()
            launcherTakePic.launch(mAvatarTmpFile)
        }

    }

    override fun dialogCityPickerOnSelect(city: City) {
        vm.detailPut("ref_city", city.name)
        vm.detailPut("ref_uf", city.uf)
        ui.edRefCityUf.setText(listOf(city.name, city.uf).joinToString(" - "))
    }

    private fun initAvatarTmpFile() {
        val f = File.createTempFile("tmp_img_file", ".png", requireActivity().cacheDir)
        f.createNewFile()
        f.deleteOnExit()

        mAvatarTmpFile = FileProvider.getUriForFile(requireActivity().applicationContext, "${BuildConfig.APPLICATION_ID}.fileprovider", f)
    }

    override fun onResume() {
        super.onResume()

        val avatar = vm.detail["avatar"]
        if (avatar?.isNotBlank() == true) {
            Img.loadAvatar(avatar, ui.avatar)
        }
    }

    override fun onStart() {
        super.onStart()
        DialogCityPicker.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogCityPicker.detach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}