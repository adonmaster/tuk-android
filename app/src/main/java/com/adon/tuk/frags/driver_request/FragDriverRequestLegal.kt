package com.adon.tuk.frags.driver_request

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.DriverRequestVM
import com.adon.tuk.databinding.FragDriverRequestLegalBinding
import com.adon.tuk.helpers.*
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePickerLauncher

class FragDriverRequestLegal: Fragment() {

    private var _ui: FragDriverRequestLegalBinding? = null
    private val ui get() = _ui!!
    private lateinit var vm: DriverRequestVM

    //
    private lateinit var launcherImg: ImagePickerLauncher


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        _ui = FragDriverRequestLegalBinding.inflate(inflater, container, false)
        vm = ViewModelProvider(requireActivity())[DriverRequestVM::class.java]

        // type
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1,
            listOf(
                "Atestado de antecedentes criminais", "Licença taxista",
                "Perfil Uber", "Perfil 99"
            )
        )
        ui.edType.setAdapter(adapter)
        ui.edType.setText(vm.legal["type"])
        ui.edType.setOnItemClickListener { _, _, _, _ ->
            vm.legalPut("type", ui.edType.text.toString())
        }

        //
        launcherImg = ImgProvider.launch(this) {

            val f = ImgProvider.fileFrom(requireContext(), it.uri) ?: return@launch

            vm.loading.value = true
            HttpMedia.upload(f, HttpMulti.ContentType.PNG)
                .then { j ->
                    vm.legalPut("img", j.full)
                    vm.legalPut("img_uid", j.uid)

                    Img.load(j.full, ui.imgDocument)
                    ui.imgDocument.alpha = 1.0f
                }
                .catch { reason ->
                    T.error(requireActivity(), reason)
                }
                .always {
                    vm.loading.value = false
                }
        }
        ui.imgDocument.setOnClickListener {
            launcherImg.launch(ImgProvider.config(false))
        }


        return ui.root
    }

    override fun onResume() {
        super.onResume()

        val imgUrl = vm.detail["img"]
        if (imgUrl?.isNotBlank() == true) {
            Img.load(imgUrl, ui.imgDocument)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

}