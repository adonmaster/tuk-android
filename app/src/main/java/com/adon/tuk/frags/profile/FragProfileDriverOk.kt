package com.adon.tuk.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.adon.tuk.databinding.FragProfileDriverOkBinding

class FragProfileDriverOk: Fragment() {

    //
    private var _ui: FragProfileDriverOkBinding? = null
    private val ui get() = _ui!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragProfileDriverOkBinding.inflate(inflater, container, false)
        return ui.root
    }

}