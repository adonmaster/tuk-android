package com.adon.tuk.frags.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.App
import com.adon.tuk.databinding.FragProfileDetailBinding
import com.adon.tuk.helpers.*

class FragProfileDetail: Fragment() {

    //
    private var _ui: FragProfileDetailBinding? = null
    private val ui get() = _ui!!
    private lateinit var vm: FragProfileVM

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View
    {
        _ui = FragProfileDetailBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProvider(requireActivity())[FragProfileVM::class.java]

        //
        App.user.observe(viewLifecycleOwner) {
            val user = it ?: return@observe

            Img.loadAvatar(user.avatar_thumb, ui.avatar)

            ui.edName.setText(user.name)
            ui.lblEmail.text = user.email
        }

        //
        ui.avatar.setOnClickListener {
            ImgProviderCrop.pick(requireContext(), this)
        }

        ui.edName.addTextChangedListener { ed ->
            Task.debounce("FragProfileDetail@edName", 2000) {
                if (ed?.toString() != App.user.value?.name) {
                    val name = ed?.toString() ?: return@debounce

                    App.user.value?.name = name
                    App.shared.userFlush()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImgProviderCrop.onActivityResult(requestCode, resultCode, data)?.let { f ->

            vm.loading.value = true
            HttpMedia.upload(f, HttpMulti.ContentType.PNG)
                .then {

                    App.user.value?.avatar = it.full
                    App.user.value?.avatar_thumb = it.thumb
                    App.user.value?.avatar_uid = it.uid
                    App.shared.userFlush()

                }
                .catch {
                    T.error(requireActivity(), it)
                }
                .always {
                    vm.loading.value = false
                }

        }
    }
}