package com.adon.tuk.frags.driver_request

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.adon.tuk.DriverRequestVM
import com.adon.tuk.R
import com.adon.tuk.databinding.FragDriverRequestCarBinding
import com.adon.tuk.helpers.*
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePickerLauncher
import dev.sasikanth.colorsheet.ColorSheet

class FragDriverRequestCar: Fragment() {

    private var _ui: FragDriverRequestCarBinding? = null
    private val ui get() = _ui!!
    private lateinit var vm: DriverRequestVM

    private enum class LauncherPic { NONE, CAR, DOC }

    //
    private lateinit var launcherImg: ImagePickerLauncher
    private var launcherPicLastKey = LauncherPic.NONE

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragDriverRequestCarBinding.inflate(inflater, container, false)
        vm = ViewModelProvider(requireActivity())[DriverRequestVM::class.java]

        // type
        val brands = resources.getStringArray(R.array.car_brands)
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, brands)
        ui.edBrand.setAdapter(adapter)
        ui.edBrand.setText(vm.car["brand"])
        ui.edBrand.setOnItemClickListener { _, _, _, _ ->
            vm.carPut("brand", ui.edBrand.text.toString())
        }

        //
        ui.edModel.setText(vm.car["model"])
        ui.edModel.addTextChangedListener { vm.carPut("model", it.toString()) }

        //
        val carColor = vm.car["color"]
        if (carColor != null) {
            ui.edColor.text = Span("⬤").color(carColor.toInt()).build()
        }
        ui.edColor.setOnClickListener {
            val colors = CarColors.palette.map { Color.parseColor(it) }
            ColorSheet().colorPicker(colors.toIntArray()) {
                ui.edColor.text = Span("⬤").color(it).build()
                vm.carPut("color", it.toString())
            }.show(childFragmentManager)
        }

        //
        ui.edPlate.setText(vm.car["plate"])
        ui.edPlate.addTextChangedListener { vm.carPut("plate", it.toString()) }

        //
        launcherImg = ImgProvider.launch(this) { i ->

            var key: String? = null
            var keyUid = ""
            var uiImg: ImageView? = null
            if (launcherPicLastKey == LauncherPic.CAR) {
                key = "img_car"
                keyUid = "img_car_uid"
                uiImg = ui.imgCar
            } else if (launcherPicLastKey == LauncherPic.DOC) {
                key = "img_doc"
                keyUid = "img_doc_uid"
                uiImg = ui.imgDocument
            }
            if (key == null) return@launch

            val f = ImgProvider.fileFrom(requireContext(), i.uri) ?: return@launch

            vm.loading.value = true
            HttpMedia.upload(f, HttpMulti.ContentType.PNG)
                .then { j ->
                    vm.carPut(key, j.full)
                    vm.carPut(keyUid, j.uid)
                    uiImg?.alpha = 1.0f
                    Img.load(j.full, uiImg)
                }
                .catch { reason -> T.error(requireActivity(), reason) }
                .always {
                    vm.loading.value = false
                }
        }
        ui.imgDocument.setOnClickListener {
            launcherPicLastKey = LauncherPic.DOC
            launcherImg.launch(ImgProvider.config(false))
        }
        ui.imgCar.setOnClickListener {
            launcherPicLastKey = LauncherPic.CAR
            launcherImg.launch(ImgProvider.config(false))
        }

        return ui.root
    }

    override fun onResume() {
        super.onResume()

        var imgUrl = vm.detail["img_doc"]
        if (imgUrl?.isNotBlank() == true) {
            Img.load(imgUrl, ui.imgDocument)
        }

        imgUrl = vm.detail["img_car"]
        if (imgUrl?.isNotBlank() == true) {
            Img.load(imgUrl, ui.imgCar)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _ui = null
    }
}