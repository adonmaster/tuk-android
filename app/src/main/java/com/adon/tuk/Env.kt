package com.adon.tuk

object Env {

    val MEDIA_URL = "http://ec2-3-145-34-206.us-east-2.compute.amazonaws.com:4002/api/"

    fun versionName(): String {
        val info = App.shared.packageManager.getPackageInfo(App.shared.packageName, 0)
        return info.versionName
    }

}