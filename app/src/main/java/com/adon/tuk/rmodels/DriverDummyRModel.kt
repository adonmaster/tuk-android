package com.adon.tuk.rmodels

import android.os.Parcel
import android.os.Parcelable
import com.adon.tuk.helpers.Dt
import org.threeten.bp.LocalDate

class DriverDummyRModel(
    val id: Int,
    val avatar: String,
    val name: String,
    val arrival: Int,
    val like: Int,
    val dislike: Int,
    val proposal_value: Double,
    val memberSince: LocalDate
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readDouble(),
        Dt.fromJson(parcel.readString()!!)!!.toLocalDate()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(avatar)
        parcel.writeString(name)
        parcel.writeInt(arrival)
        parcel.writeInt(like)
        parcel.writeInt(dislike)
        parcel.writeDouble(proposal_value)
        parcel.writeString(Dt.formatToJson(memberSince.atTime(0, 0)))
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DriverDummyRModel> {
        override fun createFromParcel(parcel: Parcel): DriverDummyRModel {
            return DriverDummyRModel(parcel)
        }

        override fun newArray(size: Int): Array<DriverDummyRModel?> {
            return arrayOfNulls(size)
        }
    }


}