package com.adon.tuk.repo

import com.adon.tuk.db.DB
import com.adon.tuk.models.City
import com.adon.tuk.models.CityDao

class CityRepo: BaseRepo<City, CityDao>(DB.i.city) {

    fun hasAny(): Boolean {
        return dao.hasAny()[0] > 0
    }

    fun seed(cities: List<City>) {
        dao.seed(cities)
    }

    fun query(s: String, prepend: String="%", append: String="%"): List<City> {
        return dao.query("$prepend${s.lowercase().trim()}$append")
    }

}