package com.adon.tuk.repo

import com.adon.tuk.models.BaseDao
import com.adon.tuk.models.BaseModel

abstract class BaseRepo<M: BaseModel, out D: BaseDao<M>>(protected val dao: D) {

    open fun save(model: M, refresh: Boolean=false): M {
        return if (model.id > 0) {
            dao.update(model)
            if (refresh) dao.find(model.id)!! else model
        } else {
            val id = dao.insert(model)
            dao.find(id)!!
        }
    }

}