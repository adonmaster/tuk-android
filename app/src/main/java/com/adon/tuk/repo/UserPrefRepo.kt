package com.adon.tuk.repo

import android.content.Context
import android.content.SharedPreferences
import com.adon.tuk.App
import com.adon.tuk.jmodels.JPostAd
import org.json.JSONObject

class UserPrefRepo: PrefRepo() {

    override val pref: SharedPreferences
        get() = App.shared.getSharedPreferences("main.${App.user.value?.remote_id ?: 0}.pref", Context.MODE_PRIVATE)

    fun putStatus(status: String?) {
        put("status", status ?: "")
    }

    fun getStatus(): String {
        return get("status") ?: ""
    }

    fun getDrawerStartDestination(): String {
        return get("MainActivity@drawer@start") ?: ""
    }

    fun putDrawerStartDestination(s: String?) {
        put("MainActivity@drawer@start", s ?: "")
    }

}