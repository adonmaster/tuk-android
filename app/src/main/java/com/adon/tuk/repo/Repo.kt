package com.adon.tuk.repo


object Repo {

    val pref by lazy { PrefRepo() }
    val userPref by lazy { UserPrefRepo() }
    val user by lazy { UserRepo() }
    val city by lazy { CityRepo() }
    val notification by lazy { NotificationRepo() }

}