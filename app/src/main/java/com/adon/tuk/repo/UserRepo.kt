package com.adon.tuk.repo

import com.adon.tuk.db.DB
import com.adon.tuk.models.User
import com.adon.tuk.models.UserDao
import com.adon.tuk.jmodels.JRegisterCode

class UserRepo: BaseRepo<User, UserDao>(DB.i.user) {

    fun findActive(): User? {
        return dao.findActive()
    }

    fun clearActive() {
        dao.clearActive()
    }

    fun update(userId: Long, name: String, avatarUid: String?, avatar: String?, avatarThumb: String?)
    {
        val model = dao.find(userId) ?: return
        model.name = name
        model.avatar = avatar
        model.avatar_thumb = avatarThumb
        model.avatar_uid = avatarUid

        save(model)
    }

    fun registerFrom(jmodel: JRegisterCode)
    {
        clearActive()

        val model = dao.findBy(jmodel.email) ?: User()
        model.remote_id = jmodel.id
        model.email = jmodel.email
        model.token = jmodel.token
        model.name = jmodel.name
        model.avatar = jmodel.avatar
        model.avatar_thumb = jmodel.avatar_thumb
        model.active = true

        save(model)
    }

}