package com.adon.tuk.repo

import com.adon.tuk.db.DB
import com.adon.tuk.models.NotificationModel
import com.adon.tuk.models.NotificationModelDao

class NotificationRepo: BaseRepo<NotificationModel, NotificationModelDao>(DB.i.notification) {

    fun create(user_id: Long, msg: String, key: String): NotificationModel {
        val model = NotificationModel()
        model.user_id = user_id
        model.msg = msg
        model.key = key
        return save(model)
    }

    fun countNotSeen(user_id: Long): Int {
        return dao.countNotSeen(user_id)
    }

    fun getAll(user_id: Long): List<NotificationModel> {
        return dao.getAll(user_id)
    }

    fun seen(ids: List<Long>) {
        dao.seen(ids)
    }

    fun deleteAll(user_id: Long) {
        dao.deleteAll(user_id)
    }

}