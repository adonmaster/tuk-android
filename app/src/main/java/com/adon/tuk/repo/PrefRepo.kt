package com.adon.tuk.repo

import android.content.Context
import android.content.SharedPreferences
import com.adon.tuk.App

open class PrefRepo {

    private var context: Context? = null

    private fun getContext(): Context {
        return context ?: App.shared
    }

    fun setContext(context: Context) {
        this.context = context
    }

    protected open val pref: SharedPreferences
        get() = getContext().getSharedPreferences("main.pref", Context.MODE_PRIVATE)

    fun add(key: String, v: String, position: Int?=null) {
        val r = (getList(key) ?: listOf()).toMutableList()

        r.remove(v)
        if (position!=null) {
            r.add(position, v)
        } else {
            r.add(v)
        }
        with(pref.edit()) {
            putStringSet(key, r.toSet())
            apply()
        }
    }

    fun getList(key: String): List<String>? {
        return pref.getStringSet(key, null)?.toList()
    }

    fun setList(key: String, list: List<String>) {
        pref
            .edit()
            .putStringSet(key, list.toSet())
            .apply()
    }

    fun get(key: String): String? {
        return pref.getString(key, null)
    }

    fun get(key: String, def: Int): Int {
        return pref.getInt(key, def)
    }

    fun get(key: String, def: Boolean): Boolean {
        return pref.getBoolean(key, def)
    }

    fun put(key: String, s: String) {
        pref.edit().putString(key, s).apply()
    }

    fun put(key: String, v: Int) {
        pref.edit().putInt(key, v).apply()
    }

    fun put(key: String, v: Boolean) {
        pref.edit().putBoolean(key, v).apply()
    }

    fun inc(key: String, factor: Int=1) {
        val i = get(key, 0)
        put(key, i + factor)
    }

    fun remove(key: String) {
        pref.edit().remove(key).apply()
    }

}