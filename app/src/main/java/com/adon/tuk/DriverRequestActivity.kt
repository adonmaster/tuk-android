package com.adon.tuk

import android.app.Activity
import android.os.Bundle
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.adon.tuk.helpers.*
import com.adon.tuk.frags.driver_request.FragDriverRequestCar
import com.adon.tuk.frags.driver_request.FragDriverRequestDetail
import com.adon.tuk.frags.driver_request.FragDriverRequestLegal
import com.adon.tuk.frags.driver_request.FragDriverRequestLicense
import com.adon.tuk.repo.Repo
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class DriverRequestActivity : AppCompatActivity() {

    //
    private lateinit var vm: DriverRequestVM

    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_request)

        //
        vm = ViewModelProvider(this)[DriverRequestVM::class.java]

        //
        val btnSubmit = findViewById<Button>(R.id.btn_submit)
        btnSubmit.setOnClickListener {
            request()
        }
        val doneObserver: (Boolean)->Unit = {
            btnSubmit.isEnabled = vm.detailDone.value!! && vm.licenseDone.value!!
                    && vm.legalDone.value!! && vm.carDone.value!!
        }
        vm.detailDone.observe(this, doneObserver)
        vm.licenseDone.observe(this, doneObserver)
        vm.legalDone.observe(this, doneObserver)
        vm.carDone.observe(this, doneObserver)


        //
        val progress = findViewById<ProgressBar>(R.id.progress)
        vm.loading.observe(this) {
            progress.isVisible = it
            btnSubmit.isEnabled = !it
                    && vm.detailDone.value!! && vm.licenseDone.value!!
                    && vm.legalDone.value!! && vm.carDone.value!!
        }

        setupPager()
    }

    private fun setupPager() {
        val pages = arrayOf(
            "Pessoal" to { FragDriverRequestDetail() },
            "CNH" to { FragDriverRequestLicense() },
            "Legal" to { FragDriverRequestLegal() },
            "Veículo" to { FragDriverRequestCar() }
        )
        val pager = findViewById<ViewPager2>(R.id.pager)
        val adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount() = pages.size
            override fun createFragment(position: Int) = pages[position].second()
        }
        pager.adapter = adapter

        val tabs = findViewById<TabLayout>(R.id.tab_layout)
        val tabLayoutMediator = TabLayoutMediator(tabs, pager, true) { tab, position ->
            tab.text = pages[position].first
        }
        tabLayoutMediator.attach()

        //
        val appendTitleCb: (Int, ()->Boolean)->((String)->Unit) = { pos, done ->
            { s ->
                val tab = tabs.getTabAt(pos)!!
                val title = pages[pos].first + s
                if (done()) {
                    tab.text = Span(title).color(this, R.color.md_green_500).build()
                } else {
                    tab.text = title
                }
            }
        }
        vm.detailAppendTitle.observe(this, appendTitleCb(0) { vm.detailDone.value!! })
        vm.licenseAppendTitle.observe(this, appendTitleCb(1) { vm.licenseDone.value!! })
        vm.legalAppendTitle.observe(this, appendTitleCb(2) { vm.legalDone.value!! })
        vm.carAppendTitle.observe(this, appendTitleCb(3) { vm.carDone.value!! })
    }

    private fun request()
    {
        val params = HttpFormData()
            .put("avatar", vm.detail["avatar"] ?: "")
            .put("avatar_thumb", vm.detail["avatar_thumb"] ?: "")
            .put("avatar_uid", vm.detail["avatar_uid"] ?: "")
            .put("name_first", vm.detail["name_first"] ?: "")
            .put("name_last", vm.detail["name_last"] ?: "")
            .put("gender", vm.detail["gender"] ?: "")
            .put("birth", vm.detail["birth"] ?: "")
            .put("cpf", vm.detail["cpf"] ?: "")
            .put("ref_city", vm.detail["ref_city"] ?: "")
            .put("ref_uf", vm.detail["ref_uf"] ?: "")
            .put("license_no", vm.license["no"] ?: "")
            .put("license_valid_until", vm.license["valid"] ?: "")
            .put("license_img", vm.license["img"] ?: "")
            .put("license_img_uid", vm.license["img_uid"] ?: "")
            .put("legal_type", vm.legal["type"] ?: "")
            .put("legal_img", vm.legal["img"] ?: "")
            .put("legal_img_uid", vm.legal["img_uid"] ?: "")
            .put("car_brand", vm.car["brand"] ?: "")
            .put("car_model", vm.car["model"] ?: "")
            .put("car_color", vm.car["color"] ?: "")
            .put("car_plate", vm.car["plate"] ?: "")
            .put("car_img", vm.car["img_car"] ?: "")
            .put("car_img_uid", vm.car["img_car_uid"] ?: "")
            .put("car_doc_img", vm.car["img_doc"] ?: "")
            .put("car_doc_img_uid", vm.car["img_doc_uid"] ?: "")

        vm.loading.value = true
        Http.post("driver/request", params)
            .then { json ->
                val id = json.getJSONObject("payload").getInt("id")
                Repo.userPref.put("driver_request_id", id)
                setResult(Activity.RESULT_OK)
                finish()
            }
            .catch { e -> T.error(this, e) }
            .always {
                vm.loading.value = false
            }

    }

}