package com.adon.tuk

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.adon.tuk.databinding.ActivityMainBinding
import com.adon.tuk.dialogs.DialogNotification
import com.adon.tuk.dialogs.driverAd.DialogDriverAdProposal
import com.adon.tuk.frags.driver.FragDriver
import com.adon.tuk.helpers.*
import com.adon.tuk.models.NotificationModel
import com.adon.tuk.repo.Repo
import com.adon.tuk.service.FcmService
import com.adon.tuk.service.Syncer
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), InterceptListener {

    companion object {
        var shared: MainActivity? = null
    }

    private var uiNotificationBadgeLbl: TextView? = null
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)

        val navGraph = navController.navInflater.inflate(R.navigation.nav_main)
        navGraph.startDestination = when(Repo.userPref.getDrawerStartDestination()) {
            "driver" -> R.id.nav_frag_driver
            "profile" -> R.id.nav_profile
            else -> R.id.nav_frag_customer
        }
        navController.graph = navGraph

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_frag_customer,
                R.id.nav_frag_driver,
                R.id.nav_gallery,
                R.id.nav_slideshow,
                R.id.nav_profile
            ),
            drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //
        drawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerClosed(drawerView: View) {}
            override fun onDrawerStateChanged(newState: Int) {
                navView.findViewById<FloatingActionButton>(R.id.ui_drawer_btn_profile)?.setOnClickListener {
                    findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.nav_profile)
                    drawerLayout.closeDrawers()
                    Repo.userPref.putDrawerStartDestination("profile")
                }
            }
            override fun onDrawerOpened(drawerView: View) {
                updateDrawerDesktop(navView)
                navView.findViewById<TextView>(R.id.ui_drawer_version).text = Env.versionName()
            }
        })

        updateDrawerDesktop(navView)
    }

    private fun updateDrawerDesktop(navView: NavigationView)
    {
        if (navView.findViewById<ImageView>(R.id.ui_drawer_avatar) == null) return

        //
        val img = navView.findViewById<ImageView>(R.id.ui_drawer_avatar)
        Img.loadAvatar(App.user.value?.avatar_thumb, img)

        navView.findViewById<TextView>(R.id.ui_drawer_name)?.text = App.user.value?.name
        navView.findViewById<TextView>(R.id.ui_drawer_email)?.text = App.user.value?.email
    }

    fun outUserInvalidated() {
        FcmService.setTokenSent(false)
        LoginActivity.start(this)
    }

    fun outNotify(notification: NotificationModel) {
        updateBadgeCount()
    }

    fun outUpdateNotificationBadge() {
        updateBadgeCount()
    }

    private fun updateBadgeCount() {
        val count = Repo.notification.countNotSeen(App.userId)
        uiNotificationBadgeLbl?.text = "$count"
        uiNotificationBadgeLbl?.isVisible = count > 0
    }

    private fun onNotificationBadgeClicked() {
        DialogNotification.show(supportFragmentManager)
    }

    override fun onResume() {
        super.onResume()

        // badge
        Task.main(250) {
            updateBadgeCount()
        }
    }

    override fun onStart() {
        super.onStart()
        shared = this

        //
        if (App.user.value == null) {
            outUserInvalidated()
        } else {

            when(Repo.userPref.getStatus()) {
                "ad" -> AdActivity.startAd(this)
            }

        }

        //
        FcmService.mainActivityStart()
        Syncer.registerUserStatus()
        Intercept.register(this)

    }

    override fun onStop() {
        super.onStop()

        Intercept.unregister(this)
        shared = null
    }

    override fun intercept(key: String): Any? {
        if (key=="MainActivity@isActive") return true
        return null
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        val m = menu.findItem(R.id.action_notification)

        m.actionView.setOnClickListener { onNotificationBadgeClicked() }
        uiNotificationBadgeLbl = m.actionView.findViewById(R.id.lbl_badge_notification)

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun outFcmNotify(data: Map<String, String>) {
        val payload = (data["payload"] ?: "").split(":")
        when(payload[0]) {
            "ad", "ad_remove" -> FragDriver.shared?.outRequestAd()
            "ad_proposal" -> DialogDriverAdProposal.shared?.outRequestProposal(payload[1].toInt())
        }
    }

    fun outNavigateProfile() {
        findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.nav_profile)
    }

}