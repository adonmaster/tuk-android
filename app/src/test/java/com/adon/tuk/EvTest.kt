package com.adon.tuk

import com.adon.tuk.helpers.Ev
import org.junit.Test
import org.junit.Assert.*

class EvTest {

    @Test
    fun doTheThing() {
        var i = 0
        Ev.register("chapeta", this) {
            i++
        }
        Ev.fireSynced("chapeta", null)
        Ev.fireSynced("chapeta", null)
        assertEquals(i, 2)

        Ev.unregister(this)
        Ev.fireSynced("chapeta", null)
        assertEquals(i, 2)

        Ev.register("gloria", this) {
            i = it as Int
        }
        Ev.fireSynced("gloria", 45)
        assertEquals(i, 45)
        Ev.unregister(this)

        Ev.fireSynced("gloria", 21)
        assertEquals(i, 45)
    }

}