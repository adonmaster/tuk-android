package com.adon.tuk

import com.adon.tuk.helpers.LightCache
import org.junit.Test
import org.junit.Assert.*


class LightCacheTest {

    @Test
    fun shouldDoTheThing()
    {
        val cache = LightCache(1)
        var i = 0

        var v = cache.get("jibeira", {
            i++
            "sacana"
        }, false)

        assertEquals(v, "sacana")
        assertEquals(i, 1)

        //

        assertEquals(cache.get("jibeira", { "monumento" }, false), "sacana")
        assertEquals(cache.get("jibeira", { i++; "monumento" }, true), "monumento")
        assertEquals(i, 2)

        //
        v = cache.get("dd", { i=10; "trotsky" }, false)
        assertEquals(v, "trotsky")
        assertEquals(i, 10)

        v = cache.get("ff", { "garoto" })
        assertEquals(v, "garoto")

        v = cache.get("dd", { i=10; "trotsky" }, false)
        assertEquals(v, "trotsky")
        assertEquals(i, 10)

    }

    @Test
    fun shouldTestCapacity()
    {
        var i = 0

        val c1 = LightCache(0)
        var v = c1.get("rural", { i++;"Marco" })
        assertEquals(v, "Marco")
        assertEquals(i, 1)

        v = c1.get("rural", { i += 10; "atos" })
        assertEquals(v, "atos")
        assertEquals(i, 11)

        val c2 = LightCache(300)
        v = c2.get("rural", { i++; "fisio"})
        assertEquals(v, "fisio")
        assertEquals(i, 12)

        v = c2.get("rural", { i++; "trolha"})
        assertEquals(v, "fisio")
        assertEquals(i, 12)
    }


}